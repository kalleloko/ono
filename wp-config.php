<?php
/**
 * Baskonfiguration f�r WordPress.
 *
 * Denna fil inneh�ller f�ljande konfigurationer: Inst�llningar f�r MySQL,
 * Tabellprefix, S�kerhetsnycklar, WordPress-spr�k, och ABSPATH.
 * Mer information p� {@link http://codex.wordpress.org/Editing_wp-config.php 
 * Editing wp-config.php}. MySQL-uppgifter f�r du fr�n ditt webbhotell.
 *
 * Denna fil anv�nds av wp-config.php-genereringsskript under installationen.
 * Du beh�ver inte anv�nda webbplatsen, du kan kopiera denna fil direkt till
 * "wp-config.php" och fylla i v�rdena.
 *
 * @package WordPress
 */

// ** MySQL-inst�llningar - MySQL-uppgifter f�r du fr�n ditt webbhotell ** //
/** Namnet p� databasen du vill anv�nda f�r WordPress */
define('DB_NAME', 'ono');

/** MySQL-databasens anv�ndarnamn */
define('DB_USER', 'root');

/** MySQL-databasens l�senord */
define('DB_PASSWORD', 'ett2tre4fem');

/** MySQL-server */
define('DB_HOST', 'localhost');

/** Teckenkodning f�r tabellerna i databasen. */
define('DB_CHARSET', 'utf8');

/** Kollationeringstyp f�r databasen. �ndra inte om du �r os�ker. */
define('DB_COLLATE', '');

/**#@+
 * Unika autentiseringsnycklar och salter.
 *
 * �ndra dessa till unika fraser!
 * Du kan generera nycklar med {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Du kan n�r som helst �ndra dessa nycklar f�r att g�ra aktiva cookies obrukbara, vilket tvingar alla anv�ndare att logga in p� nytt.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8(exc7W5%8EE4apw,Ck{4S5j_VS2cFL,1m>@|=+w=A=)0l#@ET#_6oIpL1PM73Gi');
define('SECURE_AUTH_KEY',  '+R<R;c5/GwkLG|M|RAPVmz}5Z}QNo>Z{F:/q92M!ue|Q/5G,;P8xCdHG9K7hu4pE');
define('LOGGED_IN_KEY',    '[)f+{+^]#W7_9X-7RPC1)DJ_G|U-]3Kw~I<t-=}dLz-%m-h>S;n~umqb^RS~|+2J');
define('NONCE_KEY',        'ijjza:d>~06m|6!|.PcDaDNPk]Sr|gmoc$I@;@7}OvE),6|tF1gYDzW`8Gp+9jAG');
define('AUTH_SALT',        'Q_-5*,/A WG4G0|}/V]3YT!D%p:-3Q,+RzG]0u[+xiS3g~kgg=&z@5HsnXzAi8T/');
define('SECURE_AUTH_SALT', 'A4s*xN]}ogAY47zzm-Lx`Cl|n{aM}+~|:NYy..rzlP.K}pI|gK?w}ai4k}# S$MB');
define('LOGGED_IN_SALT',   'Y^`D2m1^-+AW*G*__&G~_5w3p&^S%9:TKp|n8b:O-jQ@y,W{+Sebn(8.Jvb:)|e|');
define('NONCE_SALT',       '4?m*?9_uHlyluFEMP g|bw36sO|^?z(n_OZjP>v+st)A-+f+yr>V}*/j`l!0Y9/b');

/**#@-*/

/**
 * Tabellprefix f�r WordPress Databasen.
 *
 * Du kan ha flera installationer i samma databas om du ger varje installation ett unikt
 * prefix. Endast siffror, bokst�ver och understreck!
 */
$table_prefix  = 'ono_';

/**
 * WordPress-spr�k, f�rinst�llt f�r svenska.
 *
 * Du kan �ndra detta f�r att �ndra spr�k f�r WordPress.  En motsvarande .mo-fil
 * f�r det valda spr�ket m�ste finnas i wp-content/languages. Exempel, l�gg till
 * sv_SE.mo i wp-content/languages och ange WPLANG till 'sv_SE' f�r att f� sidan
 * p� svenska.
 */
define('WPLANG', '');

/** 
 * F�r utvecklare: WordPress fels�kningsl�ge. 
 * 
 * �ndra detta till true f�r att aktivera meddelanden under utveckling. 
 * Det �r rekommderat att man som till�ggsskapare och temaskapare anv�nder WP_DEBUG 
 * i sin utvecklingsmilj�. 
 */ 
define('WP_DEBUG', true);

/* Det var allt, sluta redigera h�r! Blogga p�. */

/** Absoluta s�kv�g till WordPress-katalogen. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Anger WordPress-v�rden och inkluderade filer. */
require_once(ABSPATH . 'wp-settings.php');