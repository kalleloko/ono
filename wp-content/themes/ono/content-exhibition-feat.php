<li data-orbit-slide="post-<?php the_ID(); ?>">
	<?php echo wp_get_attachment_image( get_field('featured_image'), 'exh-big' ); ?>
	<div class="orbit-caption">

		<!-- <div class="orbit-caption-wrap"> -->
			<h4 class="upcoming exh-type-header"><?php _e( 'Upcoming Exhibition', 'reverie' ); ?></h4>
			<div class="orbit-content-wrap">
				<?php
				$artists = get_the_terms( get_the_id(), 'artist' );
				$the_artists = artist_list($artists, '', 1);
				?>
				<h3 class="exh-artists"><?php echo $the_artists; ?></h3>
				<h4 class="exh-title"><?php the_title(); ?></h4>
				<div class="intro-text"><?php echo apply_filters( 'the_content', wp_trim_words( get_field( 'intro-text' ), 20 ) ); ?></div>
				<div class="exh-meta">
					<p>
					<?php echo_date( get_field('date') ); echo ' ' . get_field('time'); ?><br/>
					<?php the_field( 'address' ); ?><br/>
					<?php 
						// if( get_field('links') ) {
						// 	while ( has_sub_field('links') ) {
						// 		echo display_link( get_sub_field('url') );
						// 		echo br();
						// 	}
						// }
					?>
					</p>
				</div>
			</div>
		<!-- </div> -->
	</div>
</li>