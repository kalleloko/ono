
<!-- <article class="exhibition past columns small-12 large-4"> -->
<article class="exhibition past">
	<div class="inner-exhibition">
		<a class="no-border-link" href="<?php the_permalink(); ?>" title="<?php _e( 'Read more', 'reverie' ) ?>">
			<?php echo wp_get_attachment_image( get_field('featured_image'), 'exh-big' ); ?>
		</a>
		<?php
		$artists = get_the_terms( get_the_id(), 'artist' );
		$the_artists = artist_list($artists);
		?>
		<h3 class="exh-artists"><?php echo $the_artists; ?></h3>
		<h4 class="exh-title"><?php the_title(); ?></h4>
		<p class="date"><?php echo_date( get_field('date') ); ?></p>
		<div class="intro-text"><?php echo apply_filters( 'the_content', wp_trim_words( get_field( 'intro-text' ), 30 ) ); ?></div>
		<?php /*<a class="read-more no-border-link" href="<?php the_permalink(); ?>" title="<?php _e( 'Read more', 'reverie' ) ?>"><span class="view"><?php _e( 'view', 'reverie' ) ?></span><span class="icon arrow-right"></span></a> */ ?>

	</div>
</article>