<article class="news-wrap">
	<div class="type-post">
		<section class="text">
			<header>
				<h3><?php the_title(); ?></h3>
				<?php
				$date = get_the_date( 'j F, Y' );
				$author = '';
				if( get_field( 'author' ) ) {
					$author = ' by ' . get_field( 'author' );
				}
				?>
				<p class="news-meta"><?php printf(__( 'posted on %1s%2s', 'reverie' ), $date, $author ); ?></p>
			</header>
			<div class="content">
				<?php the_content(); ?>
				<?php the_post_thumbnail(); ?>

				<?php
				while( has_sub_field( 'images' ) ) {
					$row_layout = get_row_layout();
					if( $row_layout == 'one_large' ) {
						echo '<div class="single news-images">';
						echo wp_get_attachment_image( get_sub_field( 'image' ), 'eightcol' );
						echo '</div>';
					} else {
						echo '<div class="row news-images">';
						while( has_sub_field('small_images') ) {
							echo '<div class="columns small-6 large-3 small-images">';
							$image_attributes = wp_get_attachment_image_src( get_sub_field( 'image' ), 'full' );
							echo '<a href="' . $image_attributes[0] . '" class="lbp_primary">';
							echo wp_get_attachment_image( get_sub_field( 'image' ), 'small-news', false, $attr );
							echo '</a>';
							echo '</div>';
						}
						echo '</div>';
					}
				}
				?>
			</div>
		</section>
	</div>
</article>