<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @subpackage Reverie
 * @since Reverie 4.0
 */
?>


<?php
if ( is_archive() ) {
	$h = 'h2';
	$archive = true;
} else {
	$h = 'h1';
	$archive = false;
}
?>

<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
	<header>
		<<?php echo $h; ?> class="entry-title">
			<?php if ($archive): ?>
				<a href="<?php the_permalink(); ?>">
					<?php the_title(); ?>
				</a>
			<?php else: ?>

				<?php the_title(); ?>
			
			<?php endif; ?>
		</<?php echo $h; ?>>
		<?php reverie_entry_meta(); ?>
	</header>
	<div class="entry-content row">
		<?php while(has_sub_field("blocks_" . LANG)): ?>
			<?php
			//continue if the block type is not active
			$row_layout = get_row_layout();
			$saved_block_types = get_option( 'active_block_types' );
			if(!in_array($row_layout, $saved_block_types)) continue;

			//print the content
			get_template_part( 'sections/section', $row_layout ); ?>
		<?php endwhile; ?>
	</div>
	<footer>
		<?php if ($archive): ?>
			<?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:', 'reverie'), 'after' => '</p></nav>' )); ?>
		<?php endif; ?>
		<p><?php the_tags(); ?></p>
	</footer>
	<?php comments_template(); ?>
</article>