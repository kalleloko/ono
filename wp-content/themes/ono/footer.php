	</div><!-- Row End -->
</section><!-- Container End -->

<?php if( !is_front_page() ) { ?>
	<footer class="full-width" role="contentinfo">
		<div class="row">
			<div class="row">
				<div class="small-12 large-4 columns">
					<div>
						<p class="margin"><?php _e( 'Join the ONO mailing list', 'reverie' ); ?></p>
						<?php
						if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 2 ); }
						?>
					</div>
				</div>
				
				<div class="small-12 large-4 columns">
					<div>
						<div class="row">
							<div class="columns small-12 large-5">
								<ul>
									<?php
									$url = $_SERVER['REQUEST_URI'];
									$city_menu_order = json_decode(CITY_MENU_ORDER);
									$sub_pages = ono_subpages();

									$city_nav_slug = '';
									foreach ($sub_pages as $slug => $name) {
										if( strpos($url, $slug) !== false ) {
											$city_nav_slug = $slug;
										}
									}

									foreach ($city_menu_order as $slug => $city) {
							    		$city_term = get_term_by( 'slug', $city, 'city' );
							    		echo '<li>';
							    		echo '<a href="' . site_url( $slug . '/' . $city_nav_slug ) . '">ONO ' . $city . '</a>';
							    		echo '</li>';
							    	}
							    	?>
						    	</ul>
							</div>
							<div class="columns small-12 large-7">
								<?php echo ono_subnav(); ?>
								<p class="margin"><?php _e( 'Follow us:', 'reverie' ); ?></p>
								<?php echo social_links('white'); ?>
							</div>
						</div>
					</div>
				</div>

				<div class="small-12 large-4 columns">
					<div>
						<p><?php _e( 'All Content ©2014 One Night Only Gallery', 'reverie' ) ?></p>
						<p class="margin"><?php printf( __( 'Designed & Developed by %1s and %2s.', 'reverie' ), '<a href="http://andersalgestam.se">Anders</a>', '<a href="http://webblokomotivet.se">Kalle</a>' ); ?><br/><br/></p>
						<p class="margin"><?php _e( 'ONO works with:', 'reverie' ) ?></p>
						<p><?php _e( 'Statens Kunstakademi Norge', 'reverie' ) ?></p>
						<p class="margin"><?php _e( 'Kunstnernes Hus', 'reverie' ) ?></p>
						<a href="http://www.khio.no/Norsk/Kunstakademiet/" class="logo icon white statens-kunstakademie"></a>
						<a href="http://www.kunstnerneshus.no/kunst/" class="logo icon white kunstneres-hus"></a>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<div id="menuOverlay"></div>
<?php } ?>

<?php wp_footer(); ?>

<script>
	(function($) {
		$(document).foundation();
	})(jQuery);
</script>
	
</body>
</html>