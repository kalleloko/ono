<?php
/*
Author: Zhen Huang
URL: http://themefortress.com/

This place is much cleaner. Put your theme specific codes here,
anything else you may wan to use plugins to keep things tidy.

*/

/*
1. lib/clean.php
    - head cleanup
	- post and images related cleaning
*/
require_once('lib/clean.php'); // do all the cleaning and enqueue here
/*
2. lib/enqueue-sass.php or enqueue-css.php
    - enqueueing scripts & styles for Sass OR CSS
    - please use either Sass OR CSS, having two enabled will ruin your weekend
*/
require_once('lib/enqueue-sass.php'); // do all the cleaning and enqueue if you Sass to customize Reverie
//require_once('lib/enqueue-css.php'); // to use CSS for customization, uncomment this line and comment the above Sass line
/*
3. lib/foundation.php
	- add pagination
	- custom walker for top-bar and related
*/
require_once('lib/foundation.php'); // load Foundation specific functions like top-bar
/*
4. lib/presstrends.php
    - add PressTrends, tracks how many people are using Reverie
*/
//require_once('lib/presstrends.php'); // load PressTrends to track the usage of Reverie across the web, comment this line if you don't want to be tracked
add_image_size( 'small-news', 127, 90, true );
add_image_size( 'exh-big', 650, 448, true );
add_image_size( 'exh-small', 293, 202, true );
add_image_size( 'fourcol-square', 293, 293, true );
add_image_size( 'eightcol', 627, 600, false );



require_once('lib/base_functions.php');
require_once('lib/theme-settings/menu-item.php');
require_once('lib/separate-logins/menu-item.php');
require_once('lib/separate-logins/add_city_column.php');
require_once('lib/separate-logins/city_manager_role.php');
require_once('lib/fields.php');
require_once('lib/custom_post_types.php');
require_once('lib/custom_taxonomies.php');
require_once('lib/form/functions.php');



//sync city terms with pages
add_action('admin_init', 'delete_unused_cityterms', 100);
function delete_unused_cityterms() {
	$cities = get_terms('city');
	//print_obj($cities);
	foreach ($cities as $city) {
		if(!get_page_by_path($city->slug)) {
			//echo $city->slug . ' finns ej';
			wp_delete_term( $city->term_id, 'city' );
		}
	}
}
//sync artist's cities meta data when saving a post
add_action( 'save_post', 'sync_post_cities' );
function sync_post_cities( $post_id ) {
	if ( !isset($_POST['post_type']) || 'exhibition' != $_POST['post_type'] ) {
        return;
    }
    //print_obj($_POST);
    $city_id = $_POST['tax_input']['city'][1];
    $city = get_term_by( 'id', $city_id, 'city' );
    //print_obj($city);

    $artists = $_POST['tax_input']['artist'];
    $artists = explode(',', $artists);
    if( !empty($artists) && !empty($artists[0] ) ) {
		foreach( $artists as $artist_name ) {
			$artist = get_term_by( 'name', $artist_name, 'artist' );
			$prev_artist_cities = get_option( 'artist_' . $artist->term_id . '_post_cities' );
			
			if( strpos($city->slug, $prev_artist_cities) === false && !empty($prev_artist_cities)) {
				$all_artist_cities = $prev_artist_cities . ',' . $city->slug;
				//echo '1';
			} else {
				$all_artist_cities = $city->slug;
				//echo '2: '. $all_artist_cities.br();
			}
			//echo $prev_artist_cities . ' ' . $artist_name . ' - ' . $all_artist_cities.br();
			// echo '$prev_artist_cities: ' . $prev_artist_cities.br();
			// echo '$artist_name: ' . $artist_name.br();
			// echo 'all_artist_cities: ' . $all_artist_cities.br();
			$check = update_option( 'artist_' . $artist->term_id . '_post_cities', $all_artist_cities );
		}
	}
}

add_action('acf/register_fields', 'my_register_fields');
function my_register_fields() {
  include_once('lib/acf-repeater/repeater.php');
}
include_once('lib/acf-flexible-content/acf-flexible-content.php');
/**********************
Add theme supports
**********************/
function reverie_theme_support() {
	// Add language supports.
	load_theme_textdomain('reverie', get_template_directory() . '/lang');
	
	// Add post thumbnail supports. http://codex.wordpress.org/Post_Thumbnails
	add_theme_support('post-thumbnails');
	// set_post_thumbnail_size(150, 150, false);
	
	// rss thingy
	add_theme_support('automatic-feed-links');
	
	// Add post formarts supports. http://codex.wordpress.org/Post_Formats
	add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));
	
	// Add menu supports. http://codex.wordpress.org/Function_Reference/register_nav_menus
	add_theme_support('menus');
	register_nav_menus(array(
		'primary' => __('Primary Navigation', 'reverie'),
		'utility' => __('Utility Navigation', 'reverie')
	));
	
	// Add custom background support
	add_theme_support( 'custom-background',
	    array(
	    'default-image' => '',  // background image default
	    'default-color' => '', // background color default (dont add the #)
	    'wp-head-callback' => '_custom_background_cb',
	    'admin-head-callback' => '',
	    'admin-preview-callback' => ''
	    )
	);
}
add_action('after_setup_theme', 'reverie_theme_support'); /* end Reverie theme support */

// create widget areas: sidebar, footer
$sidebars = array('Sidebar');
foreach ($sidebars as $sidebar) {
	register_sidebar(array('name'=> $sidebar,
		'before_widget' => '<article id="%1$s" class="row widget %2$s"><div class="small-12 columns">',
		'after_widget' => '</div></article>',
		'before_title' => '<h6><strong>',
		'after_title' => '</strong></h6>'
	));
}
$sidebars = array('Footer');
foreach ($sidebars as $sidebar) {
	register_sidebar(array('name'=> $sidebar,
		'before_widget' => '<article id="%1$s" class="large-4 columns widget %2$s">',
		'after_widget' => '</article>',
		'before_title' => '<h6><strong>',
		'after_title' => '</strong></h6>'
	));
}

// return entry meta information for posts, used by multiple loops.
function reverie_entry_meta() {
	echo '<time class="updated" datetime="'. get_the_time('c') .'" pubdate>'. sprintf(__('Posted on %s at %s.', 'reverie'), get_the_time('l, F jS, Y'), get_the_time()) .'</time>';
	echo '<p class="byline author">'. __('Written by', 'reverie') .' <a href="'. get_author_posts_url(get_the_author_meta('ID')) .'" rel="author" class="fn">'. get_the_author() .'</a></p>';
}


add_action( 'init', 'add_early_globals', 1 );
function add_early_globals() {

	// city order
	$city_menu_order = get_option( 'city_menu_order' );
	if( !empty($city_menu_order) ) {
		$city_menu_order = json_decode($city_menu_order);
		foreach ($city_menu_order as $city) {
			$city_term = get_term_by( 'slug', $city, 'city' );
			$cities[ $city ] = $city_term->name;
		}
		$cities = json_encode($cities);
		define('CITY_MENU_ORDER', $cities );
	} else {
		define('CITY_MENU_ORDER', json_encode(array()) );
	}
}

add_action( 'wp_head', 'add_globals', 1 );
add_action( 'admin_head', 'add_globals' );
function add_globals() {
	if ( function_exists( 'qtrans_getLanguage' ) ) {
		define('LANG', 'lang_' . qtrans_getLanguage());
	} else {
		define('LANG', 'lang_default');
	}

	//current city:
	define('CURRENT_CITY_SLUG', current_city());

	//social links
	switch (CURRENT_CITY_SLUG) {
		case 'oslo':
			define('FB_LINK', 'https://www.facebook.com/onogallery');
			define('TW_LINK', 'https://twitter.com/ONOgallery');
			break;

		case 'vilnius':
			define('FB_LINK', 'https://www.facebook.com/onenightonlygalleryvilnius');
			define('TW_LINK', 'https://twitter.com/ONOgallery');
			break;

		case 'zurich':
			define('FB_LINK', 'https://www.facebook.com/OneNightOnlyGalleryZurich');
			define('TW_LINK', 'https://twitter.com/ONOgallery');
			break;
		
		default:
			define('FB_LINK', '');
			define('TW_LINK', '');
			break;
	}
}
//enabling php sessions
add_action('init', 'myStartSession', 1);
add_action('wp_logout', 'myEndSession');
add_action('wp_login', 'myEndSession');

function myStartSession() {
    if(!session_id()) {
        session_start();
    }
}

function myEndSession() {
    session_destroy ();
}
//function for getting current city
function current_city() {
	global $wp_query;
	$use_session = true;
	if( isset($wp_query->post) ) {
		$post = $wp_query->post;
		$terms = wp_get_post_terms( $post->ID, 'city' );
		//if only 1 city is represented in the query:
		if(count($terms) == 1) {
			$term = $terms[0];
			return $term->slug;
		} else {
			$use_session = true;
		}
	} else {
		$use_session = true;
	}
	if( $use_session ) {
		if (!isset($_SESSION['city'])) {
			$_SESSION['city'] = 'oslo';
		}
		return $_SESSION['city'];
	}
}

//foundation shortcodes
function tool_tip_func( $atts ) {
     extract( shortcode_atts( array(
	      'title' => 'default title',
	      'tip' => 'default tip'
     ), $atts ) );
     return "<span data-tooltip class='{$class}' title='{$tip}'>{$title}</span>";
}
add_shortcode( 'tool-tip', 'tool_tip_func' );


//hide admin bar
function hide_admin_bar() {
    return false;
}
add_filter( 'show_admin_bar', 'hide_admin_bar' );


//save current language post as all languages
add_action( "save_post", 'update_meta_all_langs', 10 );
function update_meta_all_langs( $post_id) {
	//update_post_meta( $post_id, $meta_key, $meta_value, $prev_value );
	return;
	print_obj($_POST);

	if ( function_exists( 'qtrans_getLanguage' ) ) {
		$q_langs = qtrans_getSortedLanguages();
		foreach ($q_langs as $q_lang) {
			$langs[] = 'lang_' . $q_lang;
		}
	} else {
		$langs = array( 'lang_default' );
	}
	
	foreach( $langs as $lang_src ) {
		if( isset( $_POST['fields']['field_copy_lang_' . $lang_src] ) ) {
			if( $_POST['fields']['field_copy_lang_' . $lang_src][0] == 'copy' ) {
				foreach( $langs as $lang_target ) {
					//leave if we're in the source language
					if( $lang_src == $lang_target )
						continue;

					//leave if we're in automated wp custom field
					if( !strpos( $meta_key, $lang_src ) )
						continue;

					foreach ($_POST['fields']['field_blocks_' . $lang_src] as $d) {
						
					}

					echo "update_post_meta( $object_id, $meta_key_target, $_meta_value)".br();
					//echo update_post_meta( $object_id, $meta_key_target, $_meta_value);
					//echo $_meta_value.br();

				}
				
			}
		}
	}
	
}

//populate the user meta fields by cities
add_action('init', 'add_user_fields', 9999);
function add_user_fields() {

	function my_acf_load_field( $field ) {
		$cities = get_terms('city');
		$choices = array();
		foreach ($cities as $city) {
			$choices[ $city->slug ] = $city->name;
		}
	    $field['choices'] = $choices;
	 
	    return $field;
	}
	 
	// acf_load_field-{$field_name} - filter for a specific field based on it's name
	add_filter('acf/load_field/name=city', 'my_acf_load_field');

	function my_acf_load_author_field( $field ) {
		$cities = get_terms('city');
		$choices = array( '0' => '–' );
		foreach ($cities as $city) {
			$choices[ 'ONO ' . $city->name ] = 'ONO ' . $city->name;
		}
	    $field['choices'] = $choices;
	 
	    return $field;
	}
	 
	// acf_load_field-{$field_name} - filter for a specific field based on it's name
	add_filter('acf/load_field/name=author', 'my_acf_load_author_field');
}


function echo_date($date, $echo = true) {
	$date = DateTime::createFromFormat('Ymd', $date);
	if($echo)
		echo $date->format('j M Y');
	else
		return $date->format('j M Y');
}

function display_link( $url = '', $title = '' ) {
	if(empty($url)) return;
	if( strpos('http://', $url) != 0 )
		$url = 'http://' . $url;
	$display_url = str_replace('http://', '', $url);
	if( !empty($title) )
		$title = ' title="' . $title . '"';
	return "<a href='$url'$title>$display_url</a>";
}

function query_exhibitions($feat = false, $nr = 18) {
	if( $feat == 'featured' )
		$feat_value = 1;
	else {
		$feat_value = 0;
	}
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	//echo $feat_value;
	$args = array(
		//Choose ^ 'any' or from below, since 'any' cannot be in an array
		'post_type' => array(
			'exhibition',
		),
		
		//Order & Orderby Parameters
		'order'               	=> 'DESC',
		'orderby'             	=> 'meta_value',
		'meta_key'				=> 'date',
		'meta_query' => array(
            array(
                'key' => 'featured_exhibition',
                'value' => $feat_value,
                'compare' => '='
            )
        ),
		
		//Pagination Parameters
		'posts_per_page'         => $nr,
		'paged' 				 => $paged,
		
		//Taxonomy Parameters
		'tax_query' => array(
		'relation'  => 'AND',
			array(
				'taxonomy'         => 'city',
				'field'            => 'slug',
				'terms'            => CURRENT_CITY_SLUG,
				'include_children' => true,
				'operator'         => 'IN'
			),
		),
		
		//Permission Parameters -
		'perm' => 'readable',
		
		//Parameters relating to caching
		'no_found_rows'          => false,
		'cache_results'          => true,
		'update_post_term_cache' => true,
		'update_post_meta_cache' => true,
		
	);
	return $args;
}

function ono_popup( $triggertext, $popuptext = '' ) {
	if( empty($popuptext) )
		$class = ' empty';
	else 
		$class = ' with-content';

	$return = '<span class="popup-wrap' . $class . '">';
	$return .= $triggertext;
	if( !empty($popuptext) ) {
		$return .= '<span class="popup">';
		$return .= '<span class="icon line"></span>';
		$return .= $popuptext;
		$return .= '</span>';
	}
	$return .= '</span>';

	return $return;
}

function artist_list($artists,  $display = '', $max = 3) {

	$triggertext = '';
	$popuptext = '';

	if( !empty($artists) ) {
		$count = 0;

		if( count($artists) <= $max ) {
			$triggertext .= '<span class="names">';
			foreach ($artists as $artist) {
				// $comma = $count? ', ' : '';
				// $triggertext .= $comma . '<span>' . ono_name($artist->name, $display) . '</span>';
				$triggertext .= '<span>' . ono_name($artist->name, $display) . '</span>';
				$count++;
			}
			$triggertext .= '</span>';
		} else {
			$triggertext = '<span class="names">View artists</span><span class="artist-count">' . count($artists) . '</span>';
			foreach ($artists as $artist) {
				// $comma = $count? ', ' : '';
				// $popuptext .= $comma . '<span>' . ono_name($artist->name, $display) . '</span>';
				$popuptext .= '<span>' . ono_name($artist->name, $display) . '</span>';
				$count++;
			}
		}
	}
	$popup = ono_popup( $triggertext, $popuptext );
	return $popup;
}

function ono_subpages() {
	return array(
		'exhibitions' => __( 'Exhibitions', 'reverie' ),
		'archive' => __( 'Archive', 'reverie' ),
		'news' => __( 'News', 'reverie' ),
		'about' => __( 'About', 'reverie' ),
		'application' => __( 'Application', 'reverie' ),
	);
}

function ono_subnav() {
	$url = $_SERVER['REQUEST_URI'];

	$sub_pages = ono_subpages();
	
	// $city_menu_order = json_decode(CITY_MENU_ORDER);

	$return = '<ul class="sub-nav">';

	foreach ($sub_pages as $slug => $name) {

		$sub_menu_classes = array();
		if( strpos($url, $slug) !== false ) {
			$sub_menu_classes[] = 'current-menu-item active';
		}
		if( strpos($url, '/exhibition/') !== false ) {
			if( $slug == 'exhibitions' ) {
				$sub_menu_classes[] = 'current-menu-item';
			}
		}
		$return .= '<li class="' . implode(' ', $sub_menu_classes) . '">';
		$return .= '<a href="' . site_url( CURRENT_CITY_SLUG . '/' . $slug ) . '">' . $name . '</a></li>';
	}
	$return .= '</ul>';
	return $return;
}

//get an artists cities in three formats:
// $artists_cities['array']['city-slug'] => city-name // ordered by city order
// $artists_cities['list'] => city-slugs //white-space separated
// $artists_cities['comma_list'] => city-slugs //comma separated
function ono_artist_cities( $artist_term ) {
	$city_menu_order = json_decode(CITY_MENU_ORDER);
	$artists_cities = array();
	$cities_string_raw = get_field( 'artists_cities', 'artist_' . $artist_term );
	$cities_array = explode(',', $cities_string_raw);
	$artists_cities['array'] = array();
	$cities_array_ordered = array();
	foreach ($city_menu_order as $slug => $city) {
		if( in_array($slug, $cities_array) ) {
			$artists_cities['array'][ $slug ] = $city;
			$cities_array_ordered[] = $slug;
		}
	}
	$artists_cities['list'] = implode(' ', $cities_array_ordered);
	$artists_cities['comma_list'] = implode(', ', $artists_cities['array']);
	
	return $artists_cities;
}


//display artist name
function ono_name( $name,  $display = '' ) {
	$names = explode( ':', $name);
	$surname = $names[0];
	$firstnames = '';
	for( $i = 1; $i < count($names); $i++ ) {
		$firstnames .= ' ' . $names[$i];
	}
	if( $display == 'comma' ) {
		$return = $surname;
		if( !empty($firstnames) ) {
			$return .=  ', ' . $firstnames;
		}
		return $return;
	} else {
		return $firstnames . ' ' . $surname;
	}
}

function ono_shareLinks( $twitter_title = '' ) {
	global $post;
	$twitter_title = urlencode($twitter_title);
	$pageurl = get_permalink( $post->ID );
	$pagetitle = get_the_title( $post->ID );

	$output = '';
	$url = 'http://twitter.com/share?text='.$twitter_title.'&url='.$pageurl;
	$output .= '<a href="'.$url.'" target="_blank" class="icon twitter" title="Tweet this"></a>';

	$url = 'http://www.facebook.com/sharer.php?u='.$pageurl.'&t='.$pagetitle;
	$output .= '<a href="'.$url.'" target="_blank" class="icon fb" title="Share this exhibition on Facebook"></a>';

	return $output;
}

function social_links( $color = '', $before = '' ) {
	$return = '<p class="social-links">';
	$return .= $before;
		if( constant( 'FB_LINK' ) )  {
			$return .= '<a class="social fb icon ' . $color . '" href="' . FB_LINK .'"></a>';
		}
		if( constant( 'TW_LINK' ) ) {
			$return .= '<a class="social twitter icon ' . $color . '" href="' . TW_LINK . '"></a>';
		}
	$return .= '</p>';
	return $return;
}

//ninja form tweaks
function prefix_move_ninja_forms_messages() {
    remove_action( 'ninja_forms_display_before_form', 'ninja_forms_display_response_message', 10 );
    add_action( 'ninja_forms_display_after_form', 'ninja_forms_display_response_message', 10 );
}
add_action( 'wp_head', 'prefix_move_ninja_forms_messages' );




function format_link( $link ){
	if( preg_match('%^http://%', $link) === 1 ) { 
		$return['http'] = $link;
		$return['www'] = str_replace('http://', '', $link);
	} else {
		$return['http'] = 'http://' . $link;
		$return['www'] = $link;
	}
	return $return;
}

//redirects
add_action('template_redirect', 'ono_redirects', 100);
function ono_redirects() {
	global $wp_query;
	$url = $_SERVER['REQUEST_URI'];
	$url = strtok($url, '?');
	$cities = json_decode(CITY_MENU_ORDER);
	foreach( $cities as $slug => $name ) {
		if( preg_match("%$slug/?$%", $url) === 1 ) {
			wp_redirect( site_url( "/$slug/exhibitions" ) );
			exit;
		}
	}
	if( preg_match("%latest-press%", $url) === 1 ) {
		wp_redirect(home_url('/doesntexist'), 302);
		exit;
	}

	if( preg_match("%about-ono%", $url) === 1 ) {
		wp_redirect(home_url('/doesntexist'), 302);
		exit;
	}

	if( get_post_type() == 'post' && is_single() ) {
		wp_redirect(home_url('oslo/news'), 302);
		exit;
	}
}

add_filter( 'body_class', 'city_class' );
function city_class( $classes ) {
	$classes[] = CURRENT_CITY_SLUG;
	return $classes;
}

function is_localhost() {
	$c_url = $_SERVER['REQUEST_URI'];
	if( strpos( $c_url, '/ONO/') !== false ) {
		return true;
	} else {
		return false;
	}
}

function ono_alphabet() {
	return array(  
					'1-9' => '123456789',
					'A' => 'aA',
					'Ą' => 'Ąą',
					'Á' => 'áÁ',
					'B' => 'bB',
					'C' => 'cC',
					'Ć' => 'Ćć',
					'D' => 'dD',
					'E' => 'eE',
					'Ę' => 'Ęę',
					'F' => 'fF',
					'G' => 'gG',
					'H' => 'hH',
					'I' => 'iI',
					'J' => 'jJ',
					'K' => 'kK',
					'L' => 'lL',
					'Ł' => 'Łł',
					'M' => 'mM',
					'N' => 'nN',
					'Ń' => 'Ńń',
					'O' => 'oO',
					'Ó' => 'Óó',
					'P' => 'pP',
					'Q' => 'qQ',
					'R' => 'rR',
					'S' => 'sS',
					'Ś' => 'Śś',
					'T' => 'tT',
					'U' => 'UuÜü',
					'V' => 'vV',
					'W' => 'wW',
					'X' => 'xX',
					'Y' => 'yY',
					'Z' => 'zZ',
					'Ź' => 'Źź',
					'Ż' => 'Żż',
					'Å' => 'åÅ',
					'Æ' => 'Ææ',
					'Ä' => 'äÄ',
					'Ö' => 'öÖ',
					'Ø' => 'Øø',
					'Other' => '');
}

function ono_years() {
	$current_year = date("Y");
	$return = array();
	for($i = $current_year; $i >= 2008; $i--) {
		$return[$i] = $i;
	}
	return $return;
}

//make the get_terms() function arg 'name__like' query by the first letter
function old_style_name_like($clauses) {
	remove_filter('term_clauses','old_style_name_like');
	$pattern = '|(name LIKE )\'%(.+%)\'|';
	$clauses['where'] = preg_replace($pattern,'$1 \'$2\'',$clauses['where']);
	return $clauses;
}
add_filter('terms_clauses','old_style_name_like');


//add_action('init', 'dump_posts');
function dump_posts(){
	$args = array(
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'post_type' => 'exhibition'
	);
	$posts = get_posts( $args );
	var_dump($posts);
}

//shorten string by characters
function ono_split_by_chars($full_title, $chars) {
	$short_title = substr($full_title, 0, $chars);
	
	if( $short_title != $full_title ) {
		$short_title = explode(' ', $short_title);
		array_pop($short_title);
		$short_title = implode(' ', $short_title);
		$short_title .= '...';
	}
	return $short_title;
}
// function ono_get_archive_results( $show, $city) {
// 	if( $sort_by == 'artist' ) {
// 		$posts = get_terms('artist');
// 		$sorting_var = ono_alphabet();
// 		$r['popup_heading'] = __( 'Exhibitions', 'reverie' );
// 	} elseif( $sort_by == 'exhibition' ) {
// 		$args = array(
// 			'posts_per_page' => -1,
// 			'orderby' => 'title',
// 			'order' => 'ASC',
// 			'post_type' => 'exhibition'
// 		);
// 		if( $city != 'all' ) {
// 			$args['tax_query'] = array(
// 				array(
// 					'taxonomy' => 'city',
// 					'field' => 'slug',
// 					'terms' => $city
// 				)
// 			);
// 		}
// 		$posts = get_posts( $args );
// 		$sorting_var = ono_alphabet();
// 	} else { // == date
// 		$args = array(
// 			'posts_per_page' => -1,
// 			'meta_key' => 'date',
// 			'orderby' => 'meta_value_num',
// 			'order' => 'DESC',
// 			'post_type' => 'exhibition'
// 		);
// 		if( $city != 'all' ) {
// 			$args['tax_query'] = array(
// 				array(
// 					'taxonomy' => 'city',
// 					'field' => 'slug',
// 					'terms' => $city
// 				)
// 			);
// 		}
// 		$posts = get_posts( $args );
// 		$sorting_var = ono_years();
// 	}

								
// 	$popup_triggertext = '';
// 	$popuptext = '';
// 	$result = '';
// 	$nav = '';
// 	$side = 'left';


// 	$supercount = 0;
// 	///verybeginning of your page
// 	$start = microtime(true);
	 
// 	foreach ( $sorting_var as $display_sorting_var => $sorting_var_variants ) {
		
// 		$firstround = true;
		
// 		foreach( $posts as $post_id => $post ) :
	
// 			$supercount++;
// 			if( $sort_by == 'artist' ) {

// 				$full_title = trim($post->name);

// 				//check if we're supposed to leave loop
// 				$post_first_letter = mb_substr($full_title, 0, 1);
// 				if( empty($post_first_letter) ) {
// 					continue;
// 				}
// 				if( strpos($sorting_var_variants, $post_first_letter) === false && $display_sorting_var != 'Other' ) {
// 					continue;
// 				}

// 				$post_cities = ono_artist_cities( $post->term_id );
// 				if( $city != 'all' && !array_key_exists(CURRENT_CITY_SLUG, $post_cities['array']) ) {
// 					continue;
// 				}

// 				//result heading
// 				$r['heading'] = ono_name($full_title, 'comma');
// 				//text below main result heading
// 				$r['sub_heading'] = $post_cities['comma_list'];

// 				//number shown in circle
// 				$r['circle_count'] = $post->count;

// 				//list of exhibition
// 				$args = array(
// 					'posts_per_page' => -1,
// 					'post_type' => 'exhibition',
// 					'artist' => $post->slug
// 				);

// 				$posts_array = get_posts( $args );

// 				$r['popup_list'] = '';
// 				foreach( $posts_array as $post ) {
// 					$r['popup_list'] .= '<li><a href="' . $post->guid . '">' . wp_trim_words($post->post_title, 6) . '</a></li>';
// 				}
				
// 			} elseif( $sort_by == 'exhibition' ) {

// 				$full_title = trim($post->post_title);

// 				//check if we're supposed to leave loop
// 				$post_first_letter = mb_substr($full_title, 0, 1);

// 				if( empty($post_first_letter) ) {
// 					continue;
// 				}
// 				if( strpos($sorting_var_variants, $post_first_letter) === false && $display_sorting_var != 'Other' ) {
// 					continue;
// 				}

// 				$cities = wp_get_post_terms( $post->ID, 'city' );
// 				$post_cities['list'] = '';
// 				$first = true;
// 				foreach ($cities as $city_term) {
// 					if(!$first) {
// 						$post_cities['list'] .= ' ';
// 					} 
// 					$post_cities['list'] .= $city_term->name;
// 					$first = false;
// 				}

// 				$artists = wp_get_post_terms( $post->ID, 'artist' );

// 				//result heading
// 				$short_title = ono_split_by_chars($full_title, 20);

// 				$r['heading'] = $short_title . ' <span class="city">(' . $post_cities['list'] . ')</span>';
// 				//text below main result heading
// 				$r['sub_heading'] = '';
// 				$r['popup_list'] = '';

// 				$r['circle_count'] = 0;
				
// 				if( !empty($artists) ) {

// 					if( count($artists) == 1 ) {
// 						foreach ($artists as $artist) {
// 							// $comma = $count? ', ' : '';
// 							// $triggertext .= $comma . '<span>' . ono_name($artist->name, $display) . '</span>';
// 							$r['sub_heading'] .= ono_name($artist->name, 'comma');
// 							$r['circle_count']++;
// 						}
// 					} else {
// 						$r['sub_heading'] = __( 'View artists', 'reverie' );
// 						foreach ($artists as $artist) {
// 							// $comma = $count? ', ' : '';
// 							// $popuptext .= $comma . '<span>' . ono_name($artist->name, $display) . '</span>';
// 							$r['popup_list'] .= '<li>' . ono_name($artist->name, 'comma') . '</li>';
// 							$r['circle_count']++;
// 						}
// 					}
// 				}
// 				$img_attr = wp_get_attachment_image_src( get_field('featured_image', $post->ID), 'thumb' );
// 				$r['popup_heading'] = '<a href="' . get_permalink( $post->ID ) . '"><span class="img" data-img="' . $img_attr[0] . '"></span>' . br() . $full_title . '</a>'.br();
// 				$r['popup_heading'] .= __( 'Artists', 'reverie' );


// 			} else { //if $sort_by == date
				
// 				$full_title = trim($post->post_title);
// 				$date = get_field( 'date', $post->ID );

// 				//check if we're supposed to leave loop
// 				$post_first_letter = mb_substr($date, 0, 4);

// 				if( empty($post_first_letter) ) {
// 					continue;
// 				}
// 				if( strpos($sorting_var_variants, $post_first_letter) === false ) {
// 					continue;
// 				}

// 				$cities = wp_get_post_terms( $post->ID, 'city' );
// 				$post_cities['list'] = '';
// 				$first = true;
// 				foreach ($cities as $city_term) {
// 					if(!$first) {
// 						$post_cities['list'] .= ' ';
// 					} 
// 					$post_cities['list'] .= $city_term->name;
// 					$first = false;
// 				}

// 				$artists = wp_get_post_terms( $post->ID, 'artist' );

// 				//result heading
// 				$r['heading'] = echo_date($date, false);
// 				//text below main result heading
// 				$short_title = ono_split_by_chars($full_title, 30);
// 				$r['sub_heading'] = $short_title . ' (' . $post_cities['list'] . ')';

				
// 				$img_attr = wp_get_attachment_image_src( get_field('featured_image', $post->ID), 'thumb' );
// 				$r['popup_heading'] = '<a href="' . get_permalink( $post->ID ) . '"><span class="img" data-img="' . $img_attr[0] . '"></span>' . br() . $full_title . '</a>'.br();
// 				$r['popup_heading'] .= __( 'Artists', 'reverie' );

// 				$r['popup_list'] = '';
// 				$r['circle_count'] = 0;
// 				if( !empty($artists) ) {

// 					foreach ($artists as $artist) {
// 						// $comma = $count? ', ' : '';
// 						// $popuptext .= $comma . '<span>' . ono_name($artist->name, $display) . '</span>';
// 						$r['popup_list'] .= '<li>' . ono_name($artist->name, 'comma') . '</li>';
// 						$r['circle_count']++;
// 					}
// 				}

// 			}
			

// 			//first letter
// 			$side = ($side == 'left') ? 'right' : 'left';
// 			if( $firstround ) {
// 				$firstround = false;
				
// 				$class = '';
// 				if( strtolower($show) == strtolower($display_sorting_var) ) {
// 					$class = 'class="active"';
// 				}

// 				$nav .= '<li ' . $class . '><a href="' . add_query_arg( array('sort-by' => $sort_by, 'show' => $post_first_letter) ) . '">' . $display_sorting_var .'</a></li>';
				
// 				if( $show != 'all' && strpos($sorting_var_variants, $show) === false ) {
// 					continue;
// 				}
				
// 				$result .= '<li id="' . $display_sorting_var .'" class="first-letter columns small-12"><h3>' . $display_sorting_var .'</h3><a href="#title-wrap" class="show-for-small">' . __('Scroll to top', 'reverie') . '</a></li>';
// 				$side = 'left';
// 			}

// 			if( $show != 'all' && strpos($sorting_var_variants, $show) === false ) {
// 				continue;
// 			}
			

// 			//store the list item
// 			$result .= '<li class="columns small-12 large-6 archive-result-li ' . $post_cities['list'] . ' ' . $side . '">';
// 			$result .= '<div class="result-wrap">';

// 			$popup_triggertext = '<span class="result">';
// 			$popup_triggertext .= '<span class="result-heading">' . $r['heading'] . '</span>';
// 			$popup_triggertext .= '<span class="result-sub-heading">' . $r['sub_heading'] . '</span>';
// 			$popup_triggertext .= '</span>';
// 			$popup_triggertext .= $r['circle_count']? '<span class="count">' . $r['circle_count'] . '</span>' : '';
			
// 			$popuptext = '<p class="p-header">' . $r['popup_heading'] . '</p>';
// 			$popuptext .= '<ul class="exhibitions">';
// 			$popuptext .= $r['popup_list'];
// 			$popuptext .= '</ul>';

// 			$result .= ono_popup( $popup_triggertext, $popuptext );

// 			$result .= '<div class="clearfix"></div>';
// 			$result .= '</div>';
// 			$result .= '</li>';

// 			unset($posts[ $post_id ]);
		
// 		endforeach; // end post loop
		
// 	} // end sorting variable loop
	
// 	//very end of your page
// 	$end = microtime(true);
// 	print "Page generated in ".round(($end - $start), 4)." seconds";

// 	return array(
// 		'date' => array(
// 			'nav' => $nav,
// 			'result' => $result,
// 		),
// 		'exhibition' => array(
// 			'nav' => $nav,
// 			'result' => $result,
// 		),
// 		'artist' => array(
// 			'nav' => $nav,
// 			'result' => $result,
// 		),
// 	);
// }
function ono_get_archive_results( $sort_by, $show, $city ) {
	
	$posts = get_terms('artist');
	$alphabet = ono_alphabet();
	$r['popup_heading'] = __( 'Exhibitions', 'reverie' );

	$args = array(
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'post_type' => 'exhibition'
	);
	if( $sort_by == 'date' ) {
		$args['meta_key'] = 'date';
		$args['orderby'] = 'meta_value_num';
		$args['order'] = 'DESC';
	}
	// if( $city != 'all' ) {
	// 	$args['tax_query'] = array(
	// 		array(
	// 			'taxonomy' => 'city',
	// 			'field' => 'slug',
	// 			'terms' => $city
	// 		)
	// 	);
	// }
	$posts = get_posts( $args );
	
	$sorting_var = ono_years();

								
	$popup_triggertext = '';
	$popuptext = '';
	$result = array();
	$nav = array(
		'date'		=> array(),
		'artist' 	=> array(),
		'exhibition'	=> array(),
	);

	$nav['artist'] = ono_alphabet();
	$keys = array_keys($nav['artist']);
	$values = array_fill(0, count($keys), null);
	$nav['artist'] = array_combine($keys, $values);

	$nav['exhibition'] = ono_alphabet();
	$keys = array_keys($nav['exhibition']);
	$values = array_fill(0, count($keys), null);
	$nav['exhibition'] = array_combine($keys, $values);

	$side = 'left';


	$supercount = 0;
	///verybeginning of your page
	$start = microtime(true);
	
	foreach( $posts as $post_id => $post ) :

		$post_id = $post->ID;

		$post_full_title = trim( $post->post_title );
		$post_first_letter = strtoupper( mb_substr($post_full_title, 0, 1) );

		if( empty($post_first_letter) ) {
			continue;
		}
		
		$post_artists = wp_get_post_terms( $post_id, 'artist' );

		$cities = wp_get_post_terms( $post->ID, 'city' );
		$post_cities['list'] = '';
		$first = true;
		foreach ($cities as $city_term) {
			if(!$first) {
				$post_cities['list'] .= ' ';
			} 
			$post_cities['list'] .= $city_term->name;
			$first = false;
		}

		$post_date = get_field( 'date', $post_id );
		$post_year = mb_substr($post_date, 0, 4);

		//---------------------------------date
		if( !array_key_exists( $post_year, $nav['date']) ) {
			$class = '';
			if( strtolower( $show ) == strtolower( $post_year ) ) {
				$class = 'class="active"';
			}
			$nav['date'][ $post_year ] = '<li ' . $class . '><a href="' . add_query_arg( array('sort-by' => 'date', 'show' => $post_year) ) . '">' . $post_year .'</a></li>';
		}

		if( $sort_by == 'date' ) {

			if( $show != 'all' && $post_year != $show ) {
				continue;
			}

			$r['heading'] = echo_date($post_date, false);
			$short_title = ono_split_by_chars($post_full_title, 30);
			$r['sub_heading'] = $short_title . ' (' . $post_cities['list'] . ')';

			$img_attr = wp_get_attachment_image_src( get_field('featured_image', $post_id), 'thumb' );
			$r['popup_heading'] = '<a href="' . get_permalink( $post_id ) . '"><span class="img" data-img="' . $img_attr[0] . '"></span>' . br() . $post_full_title . '</a>'.br();
			$r['popup_heading'] .= __( 'Artists', 'reverie' );

			$r['popup_list'] = '';
			$r['circle_count'] = 0;
			if( !empty($post_artists) ) {

				foreach ($post_artists as $post_artist) {
					// $comma = $count? ', ' : '';
					// $popuptext .= $comma . '<span>' . ono_name($artist->name, $display) . '</span>';
					$r['popup_list'] .= '<li>' . ono_name($post_artist->name, 'comma') . '</li>';
					$r['circle_count']++;
				}
			}

			$r_string = '<li class="columns small-12 large-6 archive-result-li">';
			$r_string .= '<div class="result-wrap">';

			$popup_triggertext = '<span class="result">';
			$popup_triggertext .= '<span class="result-heading">' . $r['heading'] . '</span>';
			$popup_triggertext .= '<span class="result-sub-heading">' . $r['sub_heading'] . '</span>';
			$popup_triggertext .= '</span>';
			$popup_triggertext .= $r['circle_count']? '<span class="count">' . $r['circle_count'] . '</span>' : '';
			
			$popuptext = '<p class="p-header">' . $r['popup_heading'] . '</p>';
			$popuptext .= '<ul class="exhibitions">';
			$popuptext .= $r['popup_list'];
			$popuptext .= '</ul>';

			$r_string .= ono_popup( $popup_triggertext, $popuptext );

			$r_string .= '<div class="clearfix"></div>';
			$r_string .= '</div>';
			$r_string .= '</li>';

			$result[ $post_year ][ $post_date ][] = $r_string;
		}

		//---------------------------------artist
		foreach ( $post_artists as $artist ) {
			$artist_name = $artist->name;
			$artist_first_letter = strtoupper( mb_substr($artist_name, 0, 1) );

			foreach( $alphabet as $display_letter => $letter_variants ) {
				if( strpos( $letter_variants, $artist_first_letter ) !== false ) {
					$class = '';
					if( strtolower( $show ) == strtolower( $display_letter ) ) {
						$class = 'class="active"';
					}
					$nav['artist'][ $display_letter ] = '<li ' . $class . '><a href="' . add_query_arg( array('sort-by' => 'artist', 'show' => $display_letter) ) . '">' . $display_letter .'</a></li>';
				}
			}

			
			if( $sort_by == 'artist' ) {

				foreach( $alphabet as $display_letter => $letter_variants ) {
					if( $show != 'all' && $display_letter != $show ) {
						continue;
					}
					if( strpos( $letter_variants, $artist_first_letter ) !== false ) {
						$all_artists[ $display_letter ][ $artist->name ][ $post_id ]['post_full_title'] = $post_full_title;
						$all_artists[ $display_letter ][ $artist->name ][ $post_id ]['post_url'] = get_permalink( $post_id );

						$all_artists[ $display_letter ][ $artist->name ][ $post_id ]['artist_cities'] = $post_cities['list'];
					}
				}
			}
		}

		//------------------------------exhibition
		foreach( $alphabet as $display_letter => $letter_variants ) {

			if( strpos( $letter_variants, $post_first_letter ) !== false ) {
				$class = '';
				if( strtolower( $show ) == strtolower( $display_letter ) ) {
					$class = 'class="active"';
				}
				$nav['exhibition'][ $display_letter ] = '<li ' . $class . '><a href="' . add_query_arg( array('sort-by' => 'exhibition', 'show' => $display_letter) ) . '">' . $display_letter .'</a></li>';
				
			}
		}
		
		if( $sort_by == 'exhibition' ) {

			
			foreach( $alphabet as $display_letter => $letter_variants ) {
				if( $show != 'all' && $display_letter != $show ) {
					continue;
				}
				if( strpos( $letter_variants, $post_first_letter ) !== false ) {
					$short_title = ono_split_by_chars($post_full_title, 30);
					$all_posts[ $display_letter ][ $post_id ]['heading'] = $short_title . ' (' . $post_cities['list'] . ')';
					$all_posts[ $display_letter ][ $post_id ]['sub_heading'] = '';

					$img_attr = wp_get_attachment_image_src( get_field('featured_image', $post_id), 'thumb' );
					$all_posts[ $display_letter ][ $post_id ]['popup_heading'] = '<a href="' . get_permalink( $post_id ) . '"><span class="img" data-img="' . $img_attr[0] . '"></span>' . br() . $post_full_title . '</a>'.br();
					$all_posts[ $display_letter ][ $post_id ]['popup_heading'] .= __( 'Artists', 'reverie' );

					$all_posts[ $display_letter ][ $post_id ]['popup_list'] = '';
					$all_posts[ $display_letter ][ $post_id ]['circle_count'] = 0;
					if( !empty($post_artists) ) {

						foreach ($post_artists as $post_artist) {
							// $comma = $count? ', ' : '';
							// $popuptext .= $comma . '<span>' . ono_name($artist->name, $display) . '</span>';
							$all_posts[ $display_letter ][ $post_id ]['popup_list'] .= '<li>' . ono_name($post_artist->name, 'comma') . '</li>';
							$all_posts[ $display_letter ][ $post_id ]['circle_count']++;
						}
					}
				}
			}
		}
	
	endforeach; // end post loop
	
	//very end of your page
	

	krsort($nav['date']);

	

	$r_string = '';
	
	if( $sort_by == 'date' ) {
		foreach ($result as $year => $result_string) {
			$r_string .= '<li class="first-letter columns small-12"><h3>' . $year . '</h3><a class="show-for-small" href="#title-wrap">Scroll to top</a></li>';
			foreach( $result[ $year ] as $date => $exh ) {
				$r_string .= implode('', $result[ $year ][ $date ]);
			}
		}
	}

	if( $sort_by == 'artist' ) {
		
		
		foreach ($all_artists as $artist_first_letter => $artist_name) {
			$r_string .= '<li class="first-letter columns small-12"><h3>' . $artist_first_letter . '</h3><a class="show-for-small" href="#title-wrap">Scroll to top</a></li>';

			foreach( $artist_name as $name => $posts ) {
				$r['heading'] = ono_name($name, 'comma');
				//$short_title = ono_split_by_chars($post_full_title, 30);
				// $artist_cities = ono_artist_cities( $artist->term_id );
				
				$r['popup_heading'] = __( 'Exhibitions', 'reverie' );

				$r['sub_heading'] = '';
				$r['popup_list'] = '';
				$r['circle_count'] = count($posts);

				foreach ($posts as $post) {

					if( strpos( $r['sub_heading'], $post['artist_cities'] ) === false ) {
						if( !empty($r['sub_heading']) )
							$r['sub_heading'] .= ', ';
						$r['sub_heading'] .= $post['artist_cities'];
					}
					$r['popup_list'] .= '<li><a href="' . $post['post_url'] . '">' . $post['post_full_title'] . '</a></li>';
				}

				$r_string .= '<li class="columns small-12 large-6 archive-result-li">';
				$r_string .= '<div class="result-wrap">';

				$popup_triggertext = '<span class="result">';
				$popup_triggertext .= '<span class="result-heading">' . $r['heading'] . '</span>';
				$popup_triggertext .= '<span class="result-sub-heading">' . $r['sub_heading'] . '</span>';
				$popup_triggertext .= '</span>';
				$popup_triggertext .= $r['circle_count']? '<span class="count">' . $r['circle_count'] . '</span>' : '';
				
				$popuptext = '<p class="p-header">' . $r['popup_heading'] . '</p>';
				$popuptext .= '<ul class="exhibitions">';
				$popuptext .= $r['popup_list'];
				$popuptext .= '</ul>';

				$r_string .= ono_popup( $popup_triggertext, $popuptext );

				$r_string .= '<div class="clearfix"></div>';
				$r_string .= '</div>';
				$r_string .= '</li>';
			}
		}
	}
	if( $sort_by == 'exhibition' ) {
		$r_string = '';

		foreach( $all_posts as $first_letter => $post_id ) {
			$r_string .= '<li class="first-letter columns small-12"><h3>' . $first_letter . '</h3><a class="show-for-small" href="#title-wrap">Scroll to top</a></li>';

			foreach($post_id as $pid => $meta) {
				$r_string .= '<li class="columns small-12 large-6 archive-result-li">';
				$r_string .= '<div class="result-wrap">';

				$popup_triggertext = '<span class="result">';
				$popup_triggertext .= '<span class="result-heading">' . $meta['heading'] . '</span>';
				$popup_triggertext .= '<span class="result-sub-heading">' . $meta['sub_heading'] . '</span>';
				$popup_triggertext .= '</span>';
				$popup_triggertext .= $meta['circle_count']? '<span class="count">' . $meta['circle_count'] . '</span>' : '';
				
				$popuptext = '<p class="p-header">' . $meta['popup_heading'] . '</p>';
				$popuptext .= '<ul class="exhibitions">';
				$popuptext .= $meta['popup_list'];
				$popuptext .= '</ul>';

				$r_string .= ono_popup( $popup_triggertext, $popuptext );

				$r_string .= '<div class="clearfix"></div>';
				$r_string .= '</div>';
				$r_string .= '</li>';
			}
		}
	}

	$end = microtime(true);
	print "Page generated in ".round(($end - $start), 4)." seconds";

	return array(
		'date_nav' => implode('', $nav['date']),
		'exhibition_nav' => implode('', $nav['exhibition']),
		'artist_nav' => implode('', $nav['artist']),
		'result' => $r_string,
	);
}

add_action('registered_taxonomy', 'starttime' );
function starttime() {
	///verybeginning of your page
	$GLOBALS['start'] = microtime(true);
}
add_action('shutdown', 'endtime' );
function endtime() {

	//very end of your page
	$end = microtime(true);
	print "Page generated in ".round(($end - $GLOBALS['start']), 4)." seconds".br().br();
}

?>