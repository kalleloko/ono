<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" <?php language_attributes(); ?> "> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?> > <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">

	<title><?php echo str_replace('(Edit here) ', '', wp_title('|', false, 'right') ); bloginfo('name'); ?></title>

	<!-- Mobile viewport optimized: j.mp/bplateviewport -->
	<meta name="viewport" content="width=device-width" />

	<!-- Favicon and Feed -->
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
	<!-- <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/"> -->

	<!--  iPhone Web App Home Screen Icon -->
	<!-- <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/img/devices/reverie-icon-ipad.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/img/devices/reverie-icon-retina.png" />
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/img/devices/reverie-icon.png" /> -->

	<!-- Enable Startup Image for iOS Home Screen Web App -->
	<!-- <meta name="apple-mobile-web-app-capable" content="yes" />
	<link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/mobile-load.png" /> -->

	<!-- Startup Image iPad Landscape (748x1024) -->
	<!-- <link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/img/devices/reverie-load-ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)" /> -->
	<!-- Startup Image iPad Portrait (768x1004) -->
	<!-- <link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/img/devices/reverie-load-ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)" /> -->
	<!-- Startup Image iPhone (320x460) -->
	<!-- <link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/img/devices/reverie-load.png" media="screen and (max-device-width: 320px)" /> -->

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<div>
	<!-- Starting the Top-Bar -->
	<?php if( !is_front_page() ): ?>
		<nav class="top-bar row">
			<div id="title-wrap">
			    <ul class="title-area">
			        <li class="name">

			        	<?php $city_menu_order = json_decode(CITY_MENU_ORDER); ?>

			        	<h1><a href="<?php echo esc_url( home_url( '/' . CURRENT_CITY_SLUG . '/exhibitions/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo site_url('wp-content/themes/ono/img/One-Night-Only-logo.png') ?>" /></a></h1>
			        </li>
					<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
					
			    </ul>
			    <div class="sel-box">
			    	
			    	

			    	<span id='select' class="clearfix">
			    		<span class="current-title-city"><?php echo $city_menu_order->{CURRENT_CITY_SLUG}; ?></span>
			    		<span class="icon arrow-down"></span>
				    	<ul id="sel-option" class="city-nav toc-odd level-1" class="clearfix">
					    <?php
					    	$url = $_SERVER['REQUEST_URI'];

							$sub_pages = ono_subpages();

							// $city_nav_slug = '';
							// foreach ($sub_pages as $slug => $name) {
							// 	if( strpos($url, $slug) !== false ) {
							// 		$city_nav_slug = $slug;
							// 	}
							// }

							//the city menu
					    	foreach ($city_menu_order as $slug => $city) {
					    		$city_term = get_term_by( 'slug', $city, 'city' );
					    		$classes = array();
					    		if ( CURRENT_CITY_SLUG == $slug ) {
					    			$classes[] = 'current-city';
					    		}
					    		echo '<li class="' . implode(' ', $classes) . '">';
					    		echo '<a href="' . site_url( $slug . '/exhibitions' ) . '">' . $city . '</a>';
					    		echo '</li>';
					    	}
					    ?>
						</ul>
			    	</span>

				</div>


				<div class="toggle-topbar menu-icon"><a href="#"><span></span></a></div>
			</div>
			<p class="social-links">
				<span><?php _e( 'SHARE', 'reverie' ) ?></span><br/>
				<?php $url = 'http://www.facebook.com/sharer.php?u=' . urlencode(site_url()) . '&t=' . urlencode('ONO Gallery'); ?>
				<a class="social fb icon" target="_blank" href="<?php echo $url; ?>"></a>
				<?php $url = 'http://twitter.com/share?text='. urlencode('ONO Gallery') . '&url='. urlencode(site_url()); ?>
				<a class="social twitter icon" target="_blank" href="<?php echo $url; ?>"></a>
			</p>

			<div class="clearfix"></div>
			
			<?php echo ono_subnav(); ?>

		</nav>
	<?php endif; ?>
	<!-- End of Top-Bar -->
</div>

<!-- Start the main container -->
<section class="container" role="document">
	<div class="row">