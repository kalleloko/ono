jQuery(document).ready(function($){
	//filter artists by city
	// if( $('.city-filter').length > 0 ) {
	// 	var base = '.city-filter';
	// 	$( base + ' a').click(function(e) {
	// 		e.preventDefault();
	// 		var currActiveCities = new Array();
	// 		$('.city-filter .active').each(function(i){
	// 			currActiveCities[i] = $(this).children().attr('data-filter');
	// 		});
	// 		if( currActiveCities.length > 1 || !$(this).parent().hasClass( 'active' ) ) {
	// 			$(this).parent().toggleClass('active');
	// 		}
			
	// 		var thisCity = $(this).attr('data-filter');

	// 		if( $(this).parent().hasClass('active') ) {
	// 			$('.artist-li.' + thisCity ).fadeIn(500);
	// 			$('.first-letter.' + thisCity ).fadeIn(500);
	// 			// $('li[data-artist-cities="' + thisCity + '"]').css('backgroundColor', '#ff0000');
	// 			// $('li[data-artist-cities="' + thisCity + '"]').animate({
	// 			// 	backgroundColor: '#fff'
	// 			// }, 400, function(){});
	// 		} else {
	// 			//slide up only if the artist isn't present in any other active city
				
	// 			$('.artist-li.' + thisCity ).each(function(){
	// 				for(i=0; i<currActiveCities.length; i++) {
	// 					if( !$(this).hasClass( currActiveCities[i] ) ) {
	// 						$(this).fadeOut(300);
	// 					}
	// 				}
	// 			});
	// 		}
	// 	});
	// }
	//popups
	if( $('.popup-wrap').length > 0 ) {
		$('.popup-wrap.with-content').click(function() {
			if( $(this).hasClass('open') ) {
				$(this).children('.popup').fadeOut(100);
				$(this).removeClass('open');
			} else {
				$(this).children('.popup').fadeIn(200);
				$(this).addClass('open');
				var $imgSpan = $(this).children('span.img');
				console.log($imgSpan);
				if( $imgSpan.length > 0 ) {
					var imgSrc = $imgSpan.attr('data-img');
					$imgSpan.load(imgSrc);
				}
			}
		});
		$('.popup').mouseleave(function(){
			$(this).fadeOut(100);
			$(this).parent().removeClass('open');
		});
		$('.popup-wrap').mouseleave(function(){
			$(this).children('.popup').fadeOut(100);
			$(this).removeClass('open');
		});
	};
	//select menu in header
	if( $('#select').length > 0 ) {
		$('#menuOverlay').click(function(){
			$('#select').removeClass('open');
			$('#sel-option').hide();
			$(this).hide();
		});
		$('#select').click(function(){
			$('#select').addClass('open');
			$('#sel-option').slideDown(100);
			$('#menuOverlay').show();
		});
		$('#sel-option a').click(function(e) {
			// $('#select').html( '<span class="current-title-city">' + $(this).text() + '</span><span class="icon arrow-down"></span>');
			$('#sel-option').hide();
		})
		$('#sel-option .current-city a').click(function(e) {
			e.preventDefault();
		})
		$('#sel-option').mouseleave(function(){
			$('#select').toggleClass('open');
			$('#sel-option').hide();
			$('#sel-box').css('z-index', 100);
		});
	}

	//scroll-link
	if( $(".scroll-link").length > 0 ) {
		$(".scroll-link").click(function(event){
			event.preventDefault();
			//calculate destination place
			var dest=0;
			if($(this.hash).offset().top > $(document).height()-$(window).height()){
				dest = $(document).height()-$(window).height();
			}else{
				dest = $(this.hash).offset().top;
			}
			if( $(window).width() < 768 ) {
				dest = Math.max(dest - 250, 0);
			}
			//go to destination
			$('html,body').animate({scrollTop:dest}, 200,'swing');
		});
	};

	//fix artist navigation
	if( $('.artist-navi').length > 0 ) {
		var distance = $('.artist-navi').length ? $('.artist-navi').offset().top: 0,
		$window = $(window),
		t_top;

		//Whe resize elements of the page on windows resize. Must recalculate distance
		$(window).resize(function() {
			clearTimeout(t_top);
			t_top = setTimeout (function() {
				distance = $('.artist-navi').offset().top;
			},105);
		});
		$window.scroll(function() {
			if ($window.scrollTop() > (distance)) {
				$('.artist-navi').addClass("fixed");
				$('.artists-column').addClass('large-offset-4');
			} else if ($window.scrollTop() <= distance) {
				$('.artist-navi').removeClass("fixed");
				$('.artists-column').removeClass('large-offset-4');
			}
		});
  	}
  	if( $('.past-exhibitions').length > 0 ) {
	  	var $container = $('.past-exhibitions');
		// initialize
		$container.imagesLoaded( function() {
			$container.masonry({
				itemSelector: '.exhibition',
			});
		});
	}
	if( $('.ono-persons').length > 0 ) {
	  	var $container = $('.ono-persons');
		// initialize
		$container.imagesLoaded( function() {
			$container.masonry({
				itemSelector: '.ono-person',
			});
		});
	}

	if( $('.ninja-forms-help-text').length > 0 ) {
		$('.ninja-forms-help-text').each(function(){
			$(this).css('opacity', 0);
			$(this).after('<span class="questionmark">?</span>');
		})
	}
	$('.toggle-topbar.menu-icon').click(function(){
		$(this).parent().parent().toggleClass('expanded');
	});

	//max 2000 chars in application form textarea
	if($('.field-wrap.textarea-wrap').length == 1 ) {
		var $textArea = $('.textarea-wrap textarea');

		$textArea.bind("keyup", function(event) {
			if( $(this).val().length > 2000 ) {
				var fullText = $(this).val();
				$(this).val( fullText.substring(0, 2000) );
			}
		});

	}

	//archive sort navi effects
	if( $('.sorting-li').length > 0 ) {
		var $sortingLi = $('.sorting-li');
		$('.main-sort-link').click(function(e) {
			e.preventDefault();
		});
		$sortingLi.click( function() {
			if( !$(this).hasClass('js-open') ) {
				$(this).children('.sub-sort').slideDown( 200, function() {
					$(this).parent('.sorting-li').addClass('js-open');
				});
			} else if( !$(this).hasClass('open') ) {
				$(this).children('.sub-sort').slideUp( 200, function() {
					$(this).parent('.sorting-li').removeClass('js-open');
				});
			}
		});
	}


})