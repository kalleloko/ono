<?php
/**
 *  returns an html breakline tag
 */
if(!function_exists('br')) {
  function br() {
    return '<br/>';
  }
}
/**
 *  Print variable within <pre> tag with string: fallback
 */
if(!function_exists('print_obj')) {
  function print_obj($obj, $fallback='') {
    echo '<pre>';
    print_r($obj);
    echo $fallback;
    echo '</pre>';
  }
}

 /**
 *  Get Filtered Content By ID
 */
function the_content_by_id($post_id) {
   $page_data = get_page($post_id);
   if ( $page_data )
        return apply_filters('the_content',$page_data->post_content);
   return false;
}
/**
 *  Get Title By ID
 */
function the_title_by_id($post_id) {
   $page_data = get_page($post_id);
   if ( $page_data )
        return $page_data->post_title;
   return false;
}
/**
 *  Get ID by slug
 */
function get_ID_by_slug($page_slug) {
   $page = get_page_by_path($page_slug);
   if ($page) {
       return $page->ID;
   } else {
       return null;
   }
}
/**
 *  echoes (or returns) the first argument if it exists, with fallback
 */
function checkAndEcho($var, $fallback = '', $echo = true) {
  if( isset($var) && !empty($var) ) {
    if($echo) echo $var;
    else return $var;
  } else {
	  if($echo) echo $fallback;
	  else return $fallback;
	}
}