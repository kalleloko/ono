<?php

// Register Custom Post Type
function custom_post_type() {

	$labels = array(
		'name'                => _x( 'Exhibitions', 'Post Type General Name', 'reverie' ),
		'singular_name'       => _x( 'Exhibition', 'Post Type Singular Name', 'reverie' ),
		'menu_name'           => __( 'Exhibition', 'reverie' ),
		'parent_item_colon'   => __( 'Parent Exhibition:', 'reverie' ),
		'all_items'           => __( 'All Exhibitions', 'reverie' ),
		'view_item'           => __( 'View Exhibition', 'reverie' ),
		'add_new_item'        => __( 'Add New Exhibition', 'reverie' ),
		'add_new'             => __( 'New Exhibition', 'reverie' ),
		'edit_item'           => __( 'Edit Exhibition', 'reverie' ),
		'update_item'         => __( 'Update Exhibition', 'reverie' ),
		'search_items'        => __( 'Search Exhibitions', 'reverie' ),
		'not_found'           => __( 'No Exhibitions found', 'reverie' ),
		'not_found_in_trash'  => __( 'No Exhibitions found in Trash', 'reverie' ),
	);
	$rewrite = array(
		'slug'                => 'exhibition',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => __( 'exhibition', 'reverie' ),
		'description'         => __( 'Exhibition information pages', 'reverie' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'custom-fields' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);
	register_post_type( 'exhibition', $args );


	// $labels = array(
	// 	'name'                => _x( 'City Pages', 'Post Type General Name', 'reverie' ),
	// 	'singular_name'       => _x( 'City Page', 'Post Type Singular Name', 'reverie' ),
	// 	'menu_name'           => __( 'City Page', 'reverie' ),
	// 	'parent_item_colon'   => __( 'Parent City Page:', 'reverie' ),
	// 	'all_items'           => __( 'All City Pages', 'reverie' ),
	// 	'view_item'           => __( 'View City Page', 'reverie' ),
	// 	'add_new_item'        => __( 'Add New City Page', 'reverie' ),
	// 	'add_new'             => __( 'New City Page', 'reverie' ),
	// 	'edit_item'           => __( 'Edit City Page', 'reverie' ),
	// 	'update_item'         => __( 'Update City Page', 'reverie' ),
	// 	'search_items'        => __( 'Search City Page', 'reverie' ),
	// 	'not_found'           => __( 'No City Page found', 'reverie' ),
	// 	'not_found_in_trash'  => __( 'No City Page found in Trash', 'reverie' ),
	// );
	// $rewrite = array(
	// 	'slug'                => 'city',
	// 	'with_front'          => true,
	// 	'pages'               => true,
	// 	'feeds'               => true,
	// );
	// $args = array(
	// 	'label'               => __( 'city', 'reverie' ),
	// 	'description'         => __( 'City Page information pages', 'reverie' ),
	// 	'labels'              => $labels,
	// 	'supports'            => array( 'title', 'custom-fields', 'page-attributes' ),
	// 	'hierarchical'        => true,
	// 	'public'              => true,
	// 	'show_ui'             => true,
	// 	'show_in_menu'        => true,
	// 	'show_in_nav_menus'   => true,
	// 	'show_in_admin_bar'   => true,
	// 	'menu_position'       => 5,
	// 	'can_export'          => true,
	// 	'has_archive'         => false,
	// 	'exclude_from_search' => true,
	// 	'publicly_queryable'  => true,
	// 	'rewrite'             => $rewrite,
	// 	'capability_type'     => 'page',
	// );
	// register_post_type( 'city', $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_post_type', 0 );