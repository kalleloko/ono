<?php
// Register Custom Taxonomy
function custom_taxonomy()  {

	$labels = array(
		'name'                       => _x( 'Cities', 'Taxonomy General Name', 'reverie' ),
		'singular_name'              => _x( 'City', 'Taxonomy Singular Name', 'reverie' ),
		'menu_name'                  => __( 'City', 'reverie' ),
		'all_items'                  => __( 'All Cities', 'reverie' ),
		'parent_item'                => __( 'Parent City', 'reverie' ),
		'parent_item_colon'          => __( 'Parent City:', 'reverie' ),
		'new_item_name'              => __( 'New City Name', 'reverie' ),
		'add_new_item'               => __( 'Add New City', 'reverie' ),
		'edit_item'                  => __( 'Edit City', 'reverie' ),
		'update_item'                => __( 'Update City', 'reverie' ),
		'separate_items_with_commas' => __( 'Separate cities with commas', 'reverie' ),
		'search_items'               => __( 'Search cities', 'reverie' ),
		'add_or_remove_items'        => __( 'Add or remove cities', 'reverie' ),
		'choose_from_most_used'      => __( 'Choose from the most used cities', 'reverie' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'query_var'                  => 'city',
	);
	register_taxonomy( 'city', array('exhibition', 'page', 'post'), $args );


	$labels = array(
		'name'                       => _x( 'Artists', 'Taxonomy General Name', 'reverie' ),
		'singular_name'              => _x( 'Artist', 'Taxonomy Singular Name', 'reverie' ),
		'menu_name'                  => __( 'Artist', 'reverie' ),
		'all_items'                  => __( 'All Artists', 'reverie' ),
		'parent_item'                => __( 'Parent Artist', 'reverie' ),
		'parent_item_colon'          => __( 'Parent Artist:', 'reverie' ),
		'new_item_name'              => __( 'New Artist name', 'reverie' ),
		'add_new_item'               => __( 'Add New Artist', 'reverie' ),
		'edit_item'                  => __( 'Edit Artist', 'reverie' ),
		'update_item'                => __( 'Update Artist', 'reverie' ),
		'separate_items_with_commas' => __( 'Enter artist(s) names, separate with commas<br/><strong>Important!</strong> Format: <br/>Lastname:Firstname, Lastname:Firstname', 'reverie' ),
		'search_items'               => __( 'Search Artists', 'reverie' ),
		'add_or_remove_items'        => __( 'Add or remove Artists', 'reverie' ),
		'choose_from_most_used'      => __( 'Choose from the most used Artists', 'reverie' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'query_var'                  => 'artist',
	);
	register_taxonomy( 'artist', 'exhibition', $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_taxonomy', 0 );