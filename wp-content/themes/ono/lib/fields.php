<?php
$saved_columns = get_option( 'active_columns' );
if( empty($saved_columns) ) $saved_columns = array();
foreach($saved_columns as $col => $name) {
	$choices[$col] = $name;
}

function small_width_fields( $id ) {
	global $choices;
	return array (
		'key' => 'field_small_width_' . $id,
		'label' => __('Width on Mobile', 'reverie'),
		'name' => 'width_small',
		'type' => 'radio',
		'column_width' => 8,
		'choices' => $choices,
		'other_choice' => 0,
		'save_other_choice' => 0,
		'default_value' => 12,
		'layout' => 'vertical',
	);
}
function large_width_fields( $id ) {
	global $choices;
	return array (
		'key' => 'field_large_width_' . $id,
		'label' => __('Width on Screen', 'reverie'),
		'name' => 'width_large',
		'type' => 'radio',
		'column_width' => 8,
		'choices' => $choices,
		'other_choice' => 0,
		'save_other_choice' => 0,
		'default_value' => 12,
		'layout' => 'vertical',
	);
}

$image_sizes = array();
foreach ( get_intermediate_image_sizes() as $size) {
	$image_sizes[ $size ] = $size;
}
$fields = array();

if ( function_exists( 'qtrans_getLanguage' ) ) {
	$q_langs = qtrans_getSortedLanguages();
	foreach ($q_langs as $q_lang) {
		$langs[] = 'lang_' . $q_lang;
	}
} else {
	$langs = array( 'lang_default' );
}

$layouts = array();
$saved_block_types = get_option( 'active_block_types' );
if( empty($saved_block_types) ) $saved_block_types = array();

foreach ($langs as $lang) {
	//NEW ROW
	$layouts[] = array (
		'label' => __('New row', 'reverie'),
		'name' => 'new_row',
		'display' => 'table',
		'min' => '',
		'max' => '',
		'sub_fields' => array (
		),
	);
	//WYSIWYG
	$layouts[] = array (
		'label' => __('Rich text', 'reverie'),
		'name' => 'wysiwyg',
		'display' => 'table',
		'min' => '',
		'max' => '',
		'sub_fields' => array (
			small_width_fields( 'wysiwyg' ),
			large_width_fields( 'wysiwyg' ),
			array (
				'key' => 'field_wysiwyg',
				'label' => __('Text', 'reverie'),
				'name' => 'text_' . $lang,
				'type' => 'wysiwyg',
				'column_width' => 92,
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
		),
	);
	//IMAGES
	if( in_array('images', $saved_block_types) ) {
		$layouts[] = array (
			'label' => __('Images', 'reverie'),
			'name' => 'images',
			'display' => 'table',
			'min' => '',
			'max' => '',
			'sub_fields' => array (
				small_width_fields( 'images' ),
				large_width_fields( 'images' ),
				array (
					'key' => 'field_images_' . $lang,
					'label' => __('Images', 'reverie'),
					'name' => 'images_' . $lang,
					'type' => 'repeater',
					'column_width' => '',
					'sub_fields' => array (
						array (
							'key' => 'field_image_' . $lang,
							'label' => __('Image', 'reverie'),
							'name' => 'image_' . $lang,
							'type' => 'image',
							'column_width' => '70',
							'save_format' => 'id',
							'preview_size' => 'thumbnail',
							'library' => 'all',
						),
						array (
							'key' => 'field_image_size_' . $lang,
							'label' => __('Image Size', 'reverie'),
							'name' => 'image_size_' . $lang,
							'type' => 'select',
							'column_width' => 30,
							'choices' => $image_sizes,
							'default_value' => '',
							'allow_null' => 0,
							'multiple' => 0,
						),
					),
					'row_min' => 0,
					'row_limit' => '',
					'layout' => 'table',
					'button_label' => 'Add Image',
				),
			),
		);
	}
	//SIMPLE TABLE
	if( in_array('table', $saved_block_types) ) {
		$layouts[] = array (
			'label' => __('Simple Table', 'reverie'),
			'name' => 'table',
			'display' => 'table',
			'min' => '',
			'max' => '',
			'sub_fields' => array (
				small_width_fields( 'table' ),
				large_width_fields( 'table' ),
				array (
					'key' => 'field_tablesettings_' . $lang,
					'label' => __('Table Settings', 'reverie'),
					'name' => 'tablesettings_' . $lang,
					'type' => 'checkbox',
					'column_width' => '12',
					'choices' => array (
						'head' => __( 'First row is table heading', 'reverie' ),
						'foot' => __( 'Last row is table footer', 'reverie' ),
					),
					'default_value' => '',
					'layout' => 'horizontal',
				),
				array (
					'key' => 'field_table_' . $lang,
					'label' => __('Table rows', 'reverie'),
					'name' => 'table_rows_' . $lang,
					'type' => 'repeater',
					'column_width' => '72',
					'instructions' => __('Each row is one row in the table. Separate cells by | (a pipe symbol)', 'reverie'),
					'sub_fields' => array (
						array (
							'key' => 'field_table_row_' . $lang,
							'label' => __('Row', 'reverie'),
							'name' => 'row_' . $lang,
							'type' => 'text',
							'column_width' => '',
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'formatting' => 'html',
							'maxlength' => '',
						),
					),
					'row_min' => 0,
					'row_limit' => '',
					'layout' => 'row',
					'button_label' => 'Add Table Row',
				),
			),
		);
	}
	//MULTI-TABS
	if( in_array('multi_tabs', $saved_block_types) ) {
		$layouts[] = array (
			'label' => __('Multi-tabs', 'reverie'),
			'name' => 'multi_tabs',
			'display' => 'table',
			'min' => '',
			'max' => '',
			'sub_fields' => array (
				small_width_fields( 'tabs' ),
				large_width_fields( 'tabs' ),
				array (
					'key' => 'field_tabtypes_' . $lang,
					'label' => __('Type', 'reverie'),
					'name' => 'type_' . $lang,
					'type' => 'radio',
					'column_width' => '8',
					'choices' => array (
						'auto' => __('Titles Above, Accordion on Mobile', 'reverie'),
						'tabs' => __('Always Titles Above', 'reverie'),
						'accordion' => __('Always Accordion', 'reverie'),
						'vertical-tabs' => __('Titles to the Left', 'reverie'),
					),
					'default_value' => 'auto',
					'layout' => 'vertical',
				),
				array (
					'key' => 'field_tab_content_' . $lang,
					'label' => __('Tabs', 'reverie'),
					'name' => 'tabs_' . $lang,
					'type' => 'repeater',
					'column_width' => '76',
					'sub_fields' => array (
						array (
							'key' => 'field_tabtitle_' . $lang,
							'label' => __('Title', 'reverie'),
							'name' => 'title_' . $lang,
							'type' => 'text',
							'column_width' => '',
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'formatting' => 'html',
							'maxlength' => '',
						),
						array (
							'key' => 'field_tabcontent_' . $lang,
							'label' => __('Content', 'reverie'),
							'name' => 'content_' . $lang,
							'type' => 'wysiwyg',
							'column_width' => '',
							'default_value' => '',
							'toolbar' => 'full',
							'media_upload' => 'yes',
						),
					),
					'row_min' => 0,
					'row_limit' => '',
					'layout' => 'row',
					'button_label' => 'Add Tab',
				),
			),
		);
	}
	//ORBIT
	if( in_array('orbit', $saved_block_types) ) {
		$layouts[] = array (
			'label' => __('Image Slider', 'reverie'),
			'name' => 'orbit',
			'display' => 'table',
			'min' => '',
			'max' => '',
			'sub_fields' => array (
				small_width_fields( 'orbit' ),
				large_width_fields( 'orbit' ),
				array (
					'key' => 'field_orbit_' . $lang,
					'label' => __('Images in slider', 'reverie'),
					'name' => 'images_' . $lang,
					'type' => 'repeater',
					'column_width' => '',
					'sub_fields' => array (
						array (
							'key' => 'field_orbit_' . $lang,
							'label' => __('Image', 'reverie'),
							'name' => 'image_' . $lang,
							'type' => 'image',
							'column_width' => '70',
							'save_format' => 'id',
							'preview_size' => 'thumbnail',
							'library' => 'all',
						),
						array (
							'key' => 'field_orbit_size_' . $lang,
							'label' => __('Image Size', 'reverie'),
							'name' => 'image_size_' . $lang,
							'type' => 'select',
							'column_width' => 30,
							'choices' => $image_sizes,
							'default_value' => '',
							'allow_null' => 0,
							'multiple' => 0,
						),
						array (
							'key' => 'field_orbittitle_' . $lang,
							'label' => __('Title', 'reverie'),
							'name' => 'title_' . $lang,
							'type' => 'wysiwyg',
							'column_width' => 0,
							'default_value' => '',
							'toolbar' => 'full',
							'media_upload' => 'no',
						),
						array (
							'key' => 'field_orbitlink_' . $lang,
							'label' => __('Link to', 'reverie'),
							'name' => 'link_' . $lang,
							'type' => 'text',
							'column_width' => '',
							'default_value' => '',
							'placeholder' => 'http://',
							'prepend' => '',
							'append' => '',
							'formatting' => 'html',
							'maxlength' => '',
						),
					),
					'row_min' => 0,
					'row_limit' => '',
					'layout' => 'row',
					'button_label' => 'Add Image',
				),
			),
		);
	}
	if ( function_exists( 'qtrans_getLanguage' ) ) {
		$fields[] = array(
			'key' => 'field_lang_' . $lang,
			'label' => $q_config['windows_locale'][ str_replace('lang_', '', $lang) ],
			'name' => '',
			'type' => 'tab',
		);
	}
	// $fields[] = array (
	// 	'key' => 'field_copy_lang_' . $lang,
	// 	'label' => __('Copy this language', 'reverie'),
	// 	'name' => 'copylang_' . $lang,
	// 	'type' => 'checkbox',
	// 	'choices' => array (
	// 		'copy' => __( 'Copy this language', 'reverie' )
	// 	),
	// 	'default_value' => '',
	// 	'layout' => 'horizontal',
	// );
	$fields[] = array (
		'key' => 'field_blocks_' . $lang,
		'label' => __('Blocks', 'reverie'),
		'name' => 'blocks_' . $lang,
		'type' => 'flexible_content',
		'layouts' => $layouts,
		'button_label' => 'Add Block',
		'min' => '',
		'max' => '',
	);
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_sidlayout',
		'title' => 'Sidlayout',
		'fields' => $fields,
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'custom_fields',
			),
		),
		'menu_order' => 0,
	));
}

//======================= City managers ========================
// if(function_exists("register_field_group"))
// {
// 	register_field_group(array (
// 		'id' => 'acf_city-manager',
// 		'title' => 'City Manager',
// 		'fields' => array (
// 			array (
// 				'key' => 'field_52911f3a8b411',
// 				'label' => 'City',
// 				'name' => 'city',
// 				'type' => 'select',
// 				'choices' => $choices,
// 				'default_value' => '',
// 				'allow_null' => 1,
// 				'multiple' => 0,
// 			),
// 		),
// 		'location' => array (
// 			array (
// 				array (
// 					'param' => 'ef_user',
// 					'operator' => '==',
// 					'value' => 'city_manager',
// 					'order_no' => 0,
// 					'group_no' => 0,
// 				),
// 			),
// 			array (
// 				array (
// 					'param' => 'ef_user',
// 					'operator' => '==',
// 					'value' => 'administrator',
// 					'order_no' => 0,
// 					'group_no' => 1,
// 				),
// 			),
// 		),
// 		'options' => array (
// 			'position' => 'acf_after_title',
// 			'layout' => 'no_box',
// 			'hide_on_screen' => array (
// 			),
// 		),
// 		'menu_order' => 0,
// 	));
// }
if(function_exists("register_field_group"))
{
	// register_field_group(array (
	// 	'id' => 'acf_artist',
	// 	'title' => 'Artist',
	// 	'fields' => array (
	// 		array (
	// 			'key' => 'field_52967ca6158fc',
	// 			'label' => 'Artist\'s Links',
	// 			'name' => 'artists_links',
	// 			'type' => 'repeater',
	// 			'sub_fields' => array (
	// 				array (
	// 					'key' => 'field_529754490d763',
	// 					'label' => 'Link',
	// 					'name' => 'link',
	// 					'type' => 'text',
	// 					'column_width' => '',
	// 					'default_value' => 'http://',
	// 					'placeholder' => '',
	// 					'prepend' => '',
	// 					'append' => '',
	// 					'formatting' => 'html',
	// 					'maxlength' => '',
	// 				),
	// 			),
	// 			'row_min' => 0,
	// 			'row_limit' => '',
	// 			'layout' => 'table',
	// 			'button_label' => 'Add Link',
	// 		),
	// 	),
	// 	'location' => array (
	// 		array (
	// 			array (
	// 				'param' => 'ef_taxonomy',
	// 				'operator' => '==',
	// 				'value' => 'artist',
	// 				'order_no' => 0,
	// 				'group_no' => 0,
	// 			),
	// 		),
	// 	),
	// 	'options' => array (
	// 		'position' => 'normal',
	// 		'layout' => 'default',
	// 		'hide_on_screen' => array (
	// 		),
	// 	),
	// 	'menu_order' => 0,
	// ));
	register_field_group(array (
		'id' => 'acf_exhibition',
		'title' => 'Exhibition',
		'fields' => array (
			array (
				'key' => 'field_529e38ef1e93e',
				'label' => 'Featured exhibition',
				'name' => 'featured_exhibition',
				'type' => 'radio',
				'instructions' => 'If is set "Yes", this exhibition is displayed in the top slider of the exhibition page.',
				'required' => 1,
				'choices' => array (
					1 => 'Yes',
					0 => 'No',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => 0,
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_528e1cc6682b9',
				'label' => 'Main Image',
				'name' => 'featured_image',
				'type' => 'image',
				'instructions' => 'The thumbnail of the exhibition. Landscape orientation, minimum dimensions: 650px (width), 448px (height)',
				'required' => 1,
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_529ca9391c4d8',
				'label' => 'Description',
				'name' => 'intro-text',
				'type' => 'wysiwyg',
				'instructions' => 'UPCOMING EXHIBITIONS: Only the first 20 words of this text will be shown. PAST EXHIBITIONS: The first part of this text is shown on the front page, the full body is presented on the single exhibition page.',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
			// array (
			// 	'key' => 'field_529cab241c4d9',
			// 	'label' => 'Description',
			// 	'name' => 'description',
			// 	'type' => 'wysiwyg',
			// 	'default_value' => '',
			// 	'toolbar' => 'full',
			// 	'media_upload' => 'no',
			// ),
			array (
				'key' => 'field_54t41c4da',
				'label' => 'Videos',
				'name' => 'videos',
				'type' => 'repeater',
				//'instructions' => '',
				'sub_fields' => array (
					array (
						'key' => 'field_5egda7wfre',
						'label' => 'Video',
						'name' => 'video',
						'type' => 'textarea',
						'instructions' => 'Enter embed code from vimeo or youtube',
						'default_value' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Add Video',
			),
			array (
				'key' => 'field_529cab541c4da',
				'label' => 'Images',
				'name' => 'extra_images',
				'type' => 'repeater',
				'instructions' => 'Several images may be added at once',
				'sub_fields' => array (
					array (
						'key' => 'field_529cab8b1c4db',
						'label' => 'Images',
						'name' => 'image',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'id',
						'preview_size' => 'medium',
						'library' => 'all',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Add Images',
			),
			array (
				'key' => 'field_528e18656b9ae',
				'label' => 'Date',
				'name' => 'date',
				'type' => 'date_picker',
				'required' => 1,
				'date_format' => 'yymmdd',
				'display_format' => 'yy-mm-dd',
				'first_day' => 1,
			),
			array (
				'key' => 'field_529cda7c5859e',
				'label' => 'Time',
				'name' => 'time',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => 'HH:MM',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => 5,
			),
			array (
				'key' => 'field_529cb08b00320',
				'label' => 'Address',
				'name' => 'address',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			// array (
			// 	'key' => 'field_529e3792c5fe2',
			// 	'label' => 'Links',
			// 	'name' => 'links',
			// 	'type' => 'repeater',
			// 	'instructions' => 'Enter the url:s of webpages related to this exhibition',
			// 	'sub_fields' => array (
			// 		array (
			// 			'key' => 'field_529e37a6c5fe3',
			// 			'label' => 'URL',
			// 			'name' => 'url',
			// 			'type' => 'text',
			// 			'column_width' => '',
			// 			'default_value' => 'http://www.',
			// 			'placeholder' => '',
			// 			'prepend' => '',
			// 			'append' => '',
			// 			'formatting' => 'html',
			// 			'maxlength' => '',
			// 		),
			// 	),
			// 	'row_min' => 0,
			// 	'row_limit' => '',
			// 	'layout' => 'table',
			// 	'button_label' => 'Add Link',
			// ),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'exhibition',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'custom_fields',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_news-fields',
		'title' => 'News fields',
		'fields' => array (
			array (
				'key' => 'field_52ab559e52e6f',
				'label' => 'Author',
				'name' => 'author',
				'type' => 'select',
				'instructions' => 'Select author if applicable',
				'choices' => array (
					0 => '–',
					'ONO Oslo' => 'ONO Oslo',
					'ONO Vilnius' => 'ONO Vilnius',
					'ONO Zürich' => 'ONO Zürich',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_52ac396c2853b',
				'label' => 'Images',
				'name' => 'images',
				'type' => 'flexible_content',
				'instructions' => 'Choose between one large image, or several small images',
				'layouts' => array (
					array (
						'label' => 'One large',
						'name' => 'one_large',
						'display' => 'row',
						'min' => '',
						'max' => '',
						'sub_fields' => array (
							array (
								'key' => 'field_52ac39c72853c',
								'label' => 'Image',
								'name' => 'image',
								'type' => 'image',
								'column_width' => '',
								'save_format' => 'id',
								'preview_size' => 'large',
								'library' => 'all',
							),
						),
					),
					array (
						'label' => 'Small images',
						'name' => 'small_images',
						'display' => 'row',
						'min' => '',
						'max' => '',
						'sub_fields' => array (
							array (
								'key' => 'field_52ac39f42853e',
								'label' => 'Images',
								'name' => 'small_images',
								'type' => 'repeater',
								'column_width' => '',
								'sub_fields' => array (
									array (
										'key' => 'field_52ac3a022853f',
										'label' => 'Image',
										'name' => 'image',
										'type' => 'image',
										'column_width' => '',
										'save_format' => 'id',
										'preview_size' => 'thumbnail',
										'library' => 'all',
									),
								),
								'row_min' => 0,
								'row_limit' => '',
								'layout' => 'table',
								'button_label' => 'Add Image',
							),
						),
					),
				),
				'button_label' => 'Add Images',
				'min' => '',
				'max' => 1,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'excerpt',
				1 => 'custom_fields',
				2 => 'discussion',
				3 => 'comments',
				4 => 'author',
				5 => 'format',
				6 => 'categories',
				7 => 'tags',
				8 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_the-ono-family',
		'title' => 'The ONO family',
		'fields' => array (
			array (
				'key' => 'field_52ada37354ee8',
				'label' => 'Person',
				'name' => 'person',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_52ada3e654ee9',
						'label' => 'Image',
						'name' => 'image',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'id',
						'preview_size' => 'medium',
						'library' => 'all',
					),
					array (
						'key' => 'field_52ada3f954eea',
						'label' => 'Name',
						'name' => 'name',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_52ada40354eeb',
						'label' => 'Website',
						'name' => 'website',
						'type' => 'text',
						'column_width' => '',
						'default_value' => 'http://',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_52ada41654eec',
						'label' => 'Text',
						'name' => 'text',
						'type' => 'wysiwyg',
						'column_width' => '',
						'default_value' => '',
						'toolbar' => 'basic',
						'media_upload' => 'no',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Person',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-about-ono.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_contact-info',
		'title' => 'Contact Info',
		'fields' => array (
			array (
				'key' => 'field_52adaf968020d',
				'label' => 'City contacts',
				'name' => 'city_contacts',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_52adafbf8020e',
						'label' => 'Heading',
						'name' => 'heading',
						'type' => 'text',
						'instructions' => 'e.g. "ONO Oslo"',
						'column_width' => 25,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_52adaff28020f',
						'label' => 'Contact info',
						'name' => 'contact_info',
						'type' => 'wysiwyg',
						'instructions' => 'Email, Address, etc. (Create email-link by marking the text, click the anchor symbol and write "mailto:name@address.com")',
						'column_width' => 75,
						'default_value' => '',
						'toolbar' => 'full',
						'media_upload' => 'no',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Add city contact',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-about-ono.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'permalink',
				1 => 'the_content',
				2 => 'excerpt',
				3 => 'custom_fields',
				4 => 'discussion',
				5 => 'comments',
				6 => 'slug',
				7 => 'author',
				8 => 'format',
				9 => 'featured_image',
				10 => 'categories',
				11 => 'tags',
				12 => 'send-trackbacks',
			),
		),
		'menu_order' => 10,
	));
}
