<?php
function fr_echo_form($form_fields, $form_result){
	foreach($form_fields as $f => $v){
		if($f == 'submit_name'){
			$submit_name = $v;
			continue;
		}

		$req = false;
		if(isset($v['req']) && $v['req'] == true){
			$req = true;
		}
		$nonvalid = '';
		if(isset($_POST[$submit_name])){
			if($req){
				if(!isset($_POST[$f]) || empty($_POST[$f])){
					$nonvalid = ' non_valid';
				}
			}
			if($v['type'] == 'email'){
				if(!is_email($_POST[$f])){
					$nonvalid = ' non_valid';
				}
			}
		}

		if($v['type'] == 'text' || $v['type'] == 'email') {
			?>
			<span class="form_row <?php echo isset($v['cond'])? implode(' ', $v['cond']) : ''; echo $nonvalid; echo $req? ' req': '' ?>">
				<label for="<?php echo $f; ?>"><?php echo $v['label']; ?></label>
				<input id="<?php echo $f; ?>" name="<?php echo $f; ?>" type="text" placeholder="<?php echo $v['label']; ?>" value="<?php echo isset($_POST[$f])? $_POST[$f] : ''; ?>" />
			</span>
			<?php
		}
		if($v['type'] == 'submit') {
			?>
			<input id="<?php echo $f; ?>" name="<?php echo $f; ?>" type="submit" value="<?php echo $v['value']; ?>" />
			<?php
		}
		if($v['type'] == 'radio') { ?>
			<span class="form_row <?php echo isset($v['cond'])? implode(' ', $v['cond']) : '' ?>">
				<label for="<?php echo $f; ?>"><?php echo $v['label']; ?></label>
				<?php
				$i = 0;
				if(isset($v['selected']) && !empty($v['selected'])) {
					$selected = $v['selected'];
				} else {
					$selected = '';
				}
				foreach($v['choices'] as $c => $n){
					if($c == 'selected'){
						$selected = !empty($n)? $n : 'x';
					} else {
						?>
						<span class="radiospan">
							<input
							id="<?php echo $c; ?>"
							name="<?php echo $f; ?>"
							type="radio"
							value="<?php echo $c; ?>"
							<?php
							if($selected == '' && $i==0) echo 'checked="checked"';
							if($c == $selected) echo 'checked="checked"';
							?>
							>
							<label for="<?php echo $c; ?>"><?php echo $n;?></label>
						</span>
						<?php
						$i++;
					}
				} ?>
			</span>
			<?php
		}
	}
}
function fr_validate_form($form_fields){
	$form_result = array();
	$form_result['ok'] = true;
	if(!fr_check_form_sent($form_fields)) return false;

	foreach($form_fields as $f => $v){

		$req = false;
		if($f != 'submit_name') {
			if(isset($v['req']) && $v['req'] === true){
				$req = true;
			}
			if(isset($v['req']) && is_array($v['req'])){
				foreach($v['req'] as $name => $value){
					if($_POST[$name] == $value)
						$req = true;
				}
			}
			if($req){
				if(!isset($_POST[$f]) || empty($_POST[$f])){
					$form_result[$f]['message'][] = 'Detta fält är obligatoriskt';
					$form_result['ok'] = false;
				}
			}
		}
		if($v['type'] == 'email'){
			if(!is_email($_POST[$f])){
				$form_result[$f]['message'][] = 'Detta verkar inte vara en email-adress';
				$form_result['ok'] = false;
			}
		}
	}
	return $form_result;
}

function fr_check_form_sent($form_fields){
	return isset($_POST[$form_fields['submit_name']]);
}