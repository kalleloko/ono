<?php
function test_modify_user_table( $column ) {
    $column['city'] = 'City';
 
    return $column;
}
 
add_filter( 'manage_users_columns', 'test_modify_user_table' );
 
function add_city_user_table( $val, $column_name, $user_id ) {
    $user = get_userdata( $user_id );
 
    switch ($column_name) {
        case 'city' :
            $user_city_slug = get_user_meta( $user->ID, 'city', true );
            $user_city = get_term_by('slug', $user_city_slug, 'city');
            if(isset($user_city->name))
                $city_name = $user_city->name;
            else
                $city_name = '–';
            return $city_name;
            break;
 
        default:
    }
 
    return $return;
}
 
add_filter( 'manage_users_custom_column', 'add_city_user_table', 10, 3 );