<?php 

add_action('init', 'add_city_manager_role');
function add_city_manager_role() {
	add_role( 'city_manager', __('City Manager', 'reverie') );
	$role =& get_role('city_manager'); 
	$role->add_cap('read');
	$role->add_cap('upload_files');
	$role->add_cap('edit_posts');
	$role->add_cap('edit_others_posts');
	$role->add_cap('edit_published_posts');
	$role->add_cap('publish_posts');
	$role->add_cap('edit_pages');
	$role->add_cap('edit_others_pages');
	$role->add_cap('edit_published_pages');
	$role->add_cap('delete_posts');
	$role->add_cap('delete_others_posts');
	$role->add_cap('delete_published_posts');
	$role->add_cap('delete_private_posts');
	$role->add_cap('edit_private_posts');
	$role->add_cap('read_private_posts');
	$role->add_cap('unfiltered_upload');
}



//filter posts for city managers
add_action('pre_get_posts', 'filter_posts_by_city', 10, 5);
function filter_posts_by_city($query) {
	if( !is_admin() )
		return;
	$cu = wp_get_current_user();
	if($cu->roles[0] != 'city_manager')
		return;

	$cu_city = get_user_meta( $cu->ID, 'city', true );
	//filter posts by city
	if($query->query_vars['post_type'] == 'exhibition' ||
		$query->query_vars['post_type'] == 'post' ||
		$query->query_vars['post_type'] == 'page') {
		$tax_query = array(
			array (
				'taxonomy' => 'city',
				'field' => 'slug',
				'terms' => $cu_city,
			)
		);
		$query->set( 'tax_query', $tax_query );
	}
	//prevent direct url access to edit posts
	if(!empty($query->query_vars['post_parent'])) {
		$single_post_id = $query->query_vars['post_parent'];
		if( !has_term( $cu_city, 'city', $single_post_id ) ) {
			wp_die( __('<p>You have not access to this page.</p>', 'reverie') );
		}
	}
}

//assign correct tax term when updating posts
add_action( 'save_post', 'add_city_term' );
function add_city_term( $post_id ) {
	$cu = wp_get_current_user();
	if($cu->roles[0] != 'city_manager')
		return;
	
	$cu_city = get_user_meta( $cu->ID, 'city', true );
	wp_set_object_terms( $post_id, $cu_city, 'city', false );
}

//remove meta boxes for cities
function remove_city_meta() {
	$cu = wp_get_current_user();
	if($cu->roles[0] != 'city_manager')
		return;

	remove_meta_box( 'citydiv', 'post', 'side' );
	remove_meta_box( 'citydiv', 'exhibition', 'side' );
	remove_meta_box( 'citydiv', 'page', 'side' );
}

//add_action( 'admin_menu' , 'remove_city_meta' );

function hide_city_option() {
	$current_user = wp_get_current_user();
	$userRole = ($current_user->roles);
	if(in_array('city_manager', $userRole)) {
		echo '<style type="text/css">
			#citydiv.postbox,
			#ninja_forms_selector {
				display:none;
			}
		</style>';
	}
}

add_action('admin_head', 'hide_city_option');