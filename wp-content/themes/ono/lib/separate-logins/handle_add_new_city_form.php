<?php
check_admin_referer( 'add_new_city' );

$val = true;
$msg = '';

$new_city_name = ucfirst( $_POST['new_city_name'] );

//Check if city name is entered
if(empty($new_city_name)) {
	$val = false;
	$msg = '<div class="error"><p>' . __('A city name must be given!', 'reverie') . '</p></div>';
}
//check if city already exists
$args = array(
	'pagename'     => sanitize_title( $new_city_name ),
);
$city_test = new WP_Query( $args );

if( count($city_test->posts) > 0 ) {
	$val = false;
	$msg = '<div class="error"><p>' . sprintf(__('The City <strong>"%1s"</strong> already exists!', 'reverie'), $new_city_name) . '</p></div>';
}

//check how many cities there are
$city_menu_order = json_decode( get_option( 'city_menu_order' ) );
$count = 1;
if( !empty($city_menu_order) ) {
	foreach($city_menu_order as $slug){
		$count++;
	}
}

//Do it! Create the city pages and taxonomy!
if($val) {
	//create main city page
	$post = array(
		'menu_order'     => 0, //If new post is a page, it sets the order in which it should appear in the tabs.
		'comment_status' => 'closed', // 'closed' means no comments.
		'ping_status'    => 'closed', // 'closed' means pingbacks or trackbacks turned off
		'post_author'    => 1, //The user ID number of the author.
		'post_status'    => 'publish', //Set the status of the new post.
		'post_title'     => $new_city_name, //The title of your post.
		'post_type'      => 'page', //You may want to insert a regular post, page, link, a menu item or some custom post type 
	);

	$city_post_id = wp_insert_post( $post );
	$city_term = wp_set_object_terms( $city_post_id, $new_city_name, 'city', false );

	//create city taxonomy
	wp_insert_term( $new_city_name, 'city' );

	if( $city_post_id && $city_term ) {
		$sub_page = array();

		//exhibitions page
		$post = array(
			'menu_order'     => 2, //If new post is a page, it sets the order in which it should appear in the tabs.
			'comment_status' => 'closed', // 'closed' means no comments.
			'ping_status'    => 'closed', // 'closed' means pingbacks or trackbacks turned off
			'post_author'    => 1, //The user ID number of the author.
			'post_status'    => 'publish', //Set the status of the new post.
			'post_title'     => 'Exhibitions', //The title of your post.
			'post_parent'    => $city_post_id, //Sets the parent of the new post.
			'post_type'      => 'page', //You may want to insert a regular post, page, link, a menu item or some custom post type 
		);
		$sub_page['Exhibitions-page'] = wp_insert_post( $post );
		$sub_page['Citylink-to-exhibitions-page'] = wp_set_object_terms( $sub_page['Exhibitions-page'], $new_city_name, 'city', false );

		//artist directory page
		$post = array(
			'menu_order'     => 2, //If new post is a page, it sets the order in which it should appear in the tabs.
			'comment_status' => 'closed', // 'closed' means no comments.
			'ping_status'    => 'closed', // 'closed' means pingbacks or trackbacks turned off
			'post_author'    => 1, //The user ID number of the author.
			'post_status'    => 'publish', //Set the status of the new post.
			'post_title'     => 'Archive', //The title of your post.
			'post_parent'    => $city_post_id, //Sets the parent of the new post.
			'post_type'      => 'page', //You may want to insert a regular post, page, link, a menu item or some custom post type 
		);
		$sub_page['Archive-page'] = wp_insert_post( $post );
		$sub_page['Citylink-to-Archive-page'] = wp_set_object_terms( $sub_page['Archive-page'], $new_city_name, 'city', false );

		//news page
		$post = array(
			'menu_order'     => 1, //If new post is a page, it sets the order in which it should appear in the tabs.
			'comment_status' => 'closed', // 'closed' means no comments.
			'ping_status'    => 'closed', // 'closed' means pingbacks or trackbacks turned off
			'post_author'    => 1, //The user ID number of the author.
			'post_status'    => 'publish', //Set the status of the new post.
			'post_title'     => 'News', //The title of your post.
			'post_parent'    => $city_post_id, //Sets the parent of the new post.
			'post_type'      => 'page', //You may want to insert a regular post, page, link, a menu item or some custom post type 
		);
		$sub_page['News-page'] = wp_insert_post( $post );
		$sub_page['Citylink-to-News-page'] = wp_set_object_terms( $sub_page['News-page'], $new_city_name, 'city', false );
		
		//about page
		$post = array(
			'menu_order'     => 0, //If new post is a page, it sets the order in which it should appear in the tabs.
			'comment_status' => 'closed', // 'closed' means no comments.
			'ping_status'    => 'closed', // 'closed' means pingbacks or trackbacks turned off
			'post_author'    => 1, //The user ID number of the author.
			'post_status'    => 'publish', //Set the status of the new post.
			'post_title'     => 'About', //The title of your post.
			'post_parent'    => $city_post_id, //Sets the parent of the new post.
			'post_type'      => 'page', //You may want to insert a regular post, page, link, a menu item or some custom post type 
		);
		$sub_page['About-page'] = wp_insert_post( $post );
		$sub_page['Citylink-to-about-page'] = wp_set_object_terms( $sub_page['About-page'], $new_city_name, 'city', false );

		//application page
		$post = array(
			'menu_order'     => 2, //If new post is a page, it sets the order in which it should appear in the tabs.
			'comment_status' => 'closed', // 'closed' means no comments.
			'ping_status'    => 'closed', // 'closed' means pingbacks or trackbacks turned off
			'post_author'    => 1, //The user ID number of the author.
			'post_status'    => 'publish', //Set the status of the new post.
			'post_title'     => '(Edit here) Application ' . $new_city_name, //The title of your post.
			'post_name'		 => 'application',
			'post_parent'    => $city_post_id, //Sets the parent of the new post.
			'post_type'      => 'page', //You may want to insert a regular post, page, link, a menu item or some custom post type 
		);
		$sub_page['Application-page'] = wp_insert_post( $post );
		$sub_page['Citylink-to-application-page'] = wp_set_object_terms( $sub_page['Application-page'], $new_city_name, 'city', false );
	}
	
	$city_menu_order->$count = sanitize_title( $new_city_name );
	$city_menu_order = json_encode($city_menu_order);
	update_option( 'city_menu_order', $city_menu_order );
	
	if( in_array(0, $sub_page) || in_array(false, $sub_page) ) {
		$msg = '<div class="updated">';
		$msg .= '<p>' . sprintf(__('The City <strong>"%1s"</strong> has been added.', 'reverie'), $new_city_name) . '</p>';
		$msg .= '</div>';
	} else {
		$msg = '<div class="error"><p>' . __( 'Something went wrong.', 'reverie' ). ' ';
		foreach ($sub_page as $key => $value) {
			if( $value == 0 || $value == false ) {
				$msg .= $key . ' ' . __( 'could not be created.', 'reverie' ) . ' ';
			}
		}
		$msg = '</p></div>';
	}

}
echo $msg;