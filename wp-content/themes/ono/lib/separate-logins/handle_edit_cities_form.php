<?php
check_admin_referer( 'manage_cities' );

$val = true;
$msg = '';
$errormsg = '';
//print_obj($_POST);

$delete_city_managers = isset($_POST['delete_city_manager']) ? $_POST['delete_city_manager'] : array();
foreach ($delete_city_managers as $city => $user_id_array) {
	foreach($user_id_array as $user_id => $on) {
		update_user_meta( $user_id, 'city', '' );
		$user = get_userdata( $user_id );
		$msg .= '<p>' . sprintf(__('%1s has been removed from %2s', 'reverie'), '<em>' . $user->display_name . '</em>', $city) . '</p>';
	}
}


$posted_city_manager_ids = $_POST['new_city_manager'];
if( !empty($posted_city_manager_ids) ) {
	foreach ($posted_city_manager_ids as $city => $posted_city_manager_id) {
		if( $posted_city_manager_id == 0 ) continue;
		$users_prev_city = get_user_meta( $posted_city_manager_id, 'city', true );
		$user = get_userdata( $posted_city_manager_id );
		if( $users_prev_city != $city ) {
			if( !empty($users_prev_city) ) {
				$errormsg .= '<p>' . sprintf(__('%1s is already manager for %2s. Check the "Unassign" box for %3s in %4s and try again!', 'reverie'), '<em>' . $user->display_name . '</em>', '<strong>' . $users_prev_city . '</strong>', $user->display_name, $users_prev_city) . '</p>';
			} else {
				if($posted_city_manager_id != 0) {
					update_user_meta( $posted_city_manager_id, 'city', $city );
					$msg .= '<p>' . sprintf(__('%1s is now manager for %2s', 'reverie'), '<em>' . $user->display_name . '</em>', $city) . '</p>';
				}
			}
		} 
	}
}
$city_order = $_POST['city_order'];
$order_city = array_flip($city_order);
if( count($order_city) != count($city_order) ) {
	$errormsg .= '<p>' . __( 'The menu order of the cities has not been saved, since several cities share the same order! Give a unique order to each city and try again!', 'reverie' ) . '</p>';
} else {
	ksort($order_city);
	if( isset($_POST['delete_city']) ) {
		$i=1;
		foreach ($order_city as $number => $city_slug) {
			//echo $number . ': ' . $city_slug.br();
			echo $_POST['delete_city'].br();
			//print_obj($_POST['delete_city']);
			if( in_array( $city_slug, array_flip($_POST['delete_city']) ) ) {
				unset( $order_city[$i] );
				//print_obj($order_city);
			}
			$i++;
		}
		$i = 1;
		foreach ($order_city as $value) {
			$new_order_city[$i] = $value;
			$i++;
		}
	} else {
		$new_order_city = $order_city;
	}
	//print_obj( $order_city );
	update_option('city_menu_order', json_encode($order_city) );
}

$delete_city = isset($_POST['delete_city']) ? $_POST['delete_city'] : array();
foreach ($delete_city as $posted_city => $on) {
	$args = array(
		'name' => $posted_city,
		'post_type' => 'page',
		'posts_per_page' => 1
	);
	$city_post = get_posts( $args );

	if( $city_post ) {
		$args = array(
			'post_parent' => $city_post[0]->ID,
			'post_type' => 'page',
			'posts_per_page' => -1
		);
		$city_subposts = get_posts( $args );
		if( $city_subposts ) {
			foreach ($city_subposts as $city_subpost) {
				wp_delete_post( $city_subpost->ID, false );
			}
		}
		wp_delete_post( $city_post[0]->ID, false );

		$city_tax_terms = get_terms('city');
		foreach ($city_tax_terms as $term) {
			if( $term->slug == $posted_city ) {
				wp_delete_term( $term->term_id, 'city' );
			}
		}
		
		$city_users = get_users( array('meta_key' => 'city', 'meta_value' => $posted_city) );
		foreach( $city_users as $user ) {
			update_user_meta( $user->ID, 'city', '' );
		}
		
		$msg .= '<p>' . sprintf(__('%1s has been deleted.', 'reverie'), '<em>' . $posted_city . '</em>', $city) . '</p>';
	} else {
		$errormsg .= '<p>' . __( 'Something went wrong!', 'reverie' ) . '</p>';
	}
}


if($msg)
	echo '<div class="updated">' . $msg . '</div>';
if($errormsg)
	echo '<div class="error">' . $errormsg . '</div>';
