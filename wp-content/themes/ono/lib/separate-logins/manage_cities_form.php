<h2><?php _e( 'Manage Cities', 'reverie' ) ?></h2>


<?php if ( isset($_POST['add_new_city']) ) require_once('handle_add_new_city_form.php'); ?>
<?php if ( isset($_POST['update_cities']) ) require_once('handle_edit_cities_form.php'); ?>

<form id="new_city_form" action="" method="post">

	<?php
	
	$city_managers = get_users( array('role' => 'city_manager') );
	$admins = get_users( array('role' => 'administrator') );
	$granted_users = array_merge($admins, $city_managers);
	$city_menu_order = json_decode( get_option('city_menu_order') );
	$city_count = 0;
	if( !empty($city_menu_order) ) {
		foreach($city_menu_order as $slug){
			$city_count++;
		}
	}
	?>
	<table id="city-table">
		<tbody>
			<?php
			if( !empty($city_menu_order) ) {
				foreach ($city_menu_order as $city_slug) { ?>
					<?php
					$city_term = get_term_by('slug', $city_slug, 'city');
					$city_name = $city_term->name;
					?>
					<tr>
						<td class="city-name">
							<?php echo $city_name; ?>
						</td>


						<td class="managers-label">
							<label for="managers_<?php echo $city_slug; ?>"><?php _e( 'Managers:', 'reverie' ); ?></label>
						</td>


						<td class="city-managers">
							<ul>
								<?php foreach ($granted_users as $granted_user): ?>
									<?php if( get_user_meta( $granted_user->ID, 'city', true ) == $city_slug ) { ?>
										<li class="city_manager">
											<?php echo "<span class='name'>$granted_user->display_name <<a href='mailto:$granted_user->user_email'>$granted_user->user_email</a>></span>"; ?>
											<input type="checkbox" name="delete_city_manager[<?php echo $city_slug ?>][<?php echo $granted_user->ID ?>]" id="delete_city_manager_<?php echo $city_slug ?>_<?php echo $granted_user->ID ?>">
											<label for="delete_city_manager_<?php echo $city_slug ?>_<?php echo $granted_user->ID ?>"><?php _e( 'Unassign this user', 'reverie' ) ?></label>
										</li>
									<?php } ?>
								<?php endforeach; ?>
							</ul>
							<select name="new_city_manager[<?php echo $city_slug ?>]" id="new_city_manager[<?php echo $city_slug ?>]" class="select-manager">
								<option value="0">–</option>
								<?php
								foreach( $granted_users as $granted_user ) {
									if( get_user_meta( $granted_user->ID, 'city', true ) == $city_slug ) {
										continue;
									}
									echo "<option value='$granted_user->ID'>$granted_user->display_name</option>";
								}
								?>
							</select>
							<label for="new_city_manager[<?php echo $city_slug ?>]"><?php _e( 'Add manager', 'reverie' ) ?></label>
						</td>


						<td class="city-order">
							<label for="city_order[<?php echo $city_slug ?>]"><?php _e( 'Order in menu:', 'reverie' ); ?></label>
							<select name="city_order[<?php echo $city_slug ?>]" id="city_order[<?php echo $city_slug ?>]">
								<?php for($i=1; $i<=$city_count; $i++) {
									if($city_menu_order->{$i} == $city_slug) {
										$selected = ' selected="selected"';
									} else {
										$selected = '';
									}
									echo "<option value='$i'$selected>$i</option>";
								} ?>
							</select>
						</td>


						<?php /*<td class="delete-city">
							<input type="checkbox" id="delete_city_<?php echo $city_slug; ?>" name="delete_city[<?php echo $city_slug; ?>]">
							<label for="delete_city_<?php echo $city_slug; ?>"><?php _e( 'Delete', 'reverie' ); echo ' ' . $city_name; ?></label>
						</td>*/ ?>
					</tr>
				<?php }
				} ?>
		</tbody>
	</table>
	<?php wp_nonce_field( 'manage_cities' ); ?>
	<?php submit_button( __( 'Update Cities', 'reverie' ), 'primary', 'update_cities' ); ?>
</form>

<form id="new_city_form" action="" method="post">
	<h3><?php _e( 'Add new city', 'reverie' ); ?></h3>

	<label for="new_city_name"><?php _e( 'Name:', 'reverie' ); ?> </label>
	<input id="new_city_name" type="text" name="new_city_name" placeholder="<?php _e( 'City name', 'reverie' ); ?>" />

	<?php wp_nonce_field( 'add_new_city' ); ?>
	<?php submit_button( __( 'Add City', 'reverie' ), 'primary', 'add_new_city' ); ?>
</form>