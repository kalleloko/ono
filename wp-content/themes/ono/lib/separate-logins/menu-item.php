<?php
/** Step 2 (from text above). */
add_action( 'admin_menu', 'manage_cities_menu' );

/** Step 1. */
function manage_cities_menu() {
	$menu = add_menu_page( 'Manage Cities', 'Manage Cities', 'edit_pages', 'manage_cities', 'manage_cities_form', '', 30 );
	add_action( 'admin_print_styles-' . $menu, 'enqueue_admin_style' );
}

/** Step 3. */
function manage_cities_form() {
	if ( !current_user_can( 'edit_pages' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	echo '<div class="wrap">';
	require_once('manage_cities_form.php');
	echo '</div>';
}

function enqueue_admin_style() {
	wp_register_style( 'manage_cities_styles', get_template_directory_uri() . '/lib/separate-logins/manage_cities_styles.css', array(), '' );
	wp_enqueue_style( 'manage_cities_styles' );
}
?>