<?php
/** Step 2 (from text above). */
add_action( 'admin_menu', 'settings_menu' );

/** Step 1. */
function settings_menu() {
	add_options_page( 'Theme Settings', 'Theme Settings', 'manage_options', 'theme_settings', 'theme_settings' );
}

/** Step 3. */
function theme_settings() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	echo '<div class="wrap">';
	require_once('settings.php');
	echo '</div>';
}
?>