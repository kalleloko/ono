<?php screen_icon('themes'); ?> <h2>Theme settings</h2>  



<?php //save settings: ?>
<?php if (isset($_POST["submit"])) {
	//columns
	$active_columns = array();
	if( isset($_POST['columns']) ) {
		foreach( $_POST['columns'] as $col ) {
			$names = $_POST['columnnames'];
			$active_columns[ $col ] = $names[ $col ];
		}
	} else {
		$active_columns = array();
	}
	$colupdate = update_option( 'active_columns', $active_columns );
	//block-types
	if( isset($_POST['block_types']) ) {
		$blockupdate = update_option( 'active_block_types', $_POST['block_types'] );
	} else {
		$blockupdate = false;
	}
	//print_obj($_POST);
	//print update messages
	if($colupdate)
		echo '<div class="updated"><p>' . __( 'Your columns have been saved!', 'reverie' ) . '</p></div>';

	if($blockupdate) {
		echo '<div class="updated"><p>' . __( 'Your block types have been saved!', 'reverie' ) . '</p></div>';
	}
} ?>

<?php
$saved_columns = get_option( 'active_columns' );
$saved_block_types = get_option( 'active_block_types' );
if( empty($saved_block_types) ) $saved_block_types = array();
?>

<form method="POST" action="">
	<div>
		<h3><?php _e( 'Active Columns:', 'reverie' ) ?></h3>
		<?php
		for ($i = 1; $i <= 12; $i++) { ?>
			<?php
			$checked = '';
			$colname = $i . ' / 12';
			if(isset($saved_columns[$i])) {
				$checked = 'checked="checked"';
				$colname = $saved_columns[$i];
			}?>
			<input type="checkbox" name="columns[<?php echo $i ?>]" value="<?php echo $i ?>" id="<?php echo 'col' . $i; ?>" <?php echo $checked; ?>>
			<label for="<?php echo 'col' . $i; ?>"><?php echo $i . ' / 12'; ?></label>
			<label for="<?php echo 'colname' . $i; ?>"><?php _e( 'Column name:', 'reverie' ) ?></label>
			<input type="text" name="columnnames[<?php echo $i ?>]" id="<?php echo 'colname' . $i; ?>" value="<?php echo $colname; ?>">
			<br/>
		<?php } ?>
	</div>

	<div>
		<h3><?php _e( 'Active Block Types:', 'reverie' ) ?></h3>
		<?php
		$block_types = array(
			'images' 		=> __( 'Images', 'reverie' ),
			'multi_tabs'	=> __( 'Multi-tabs', 'reverie' ),
			'orbit'			=> __( 'Image Slider', 'reverie' ),
			'table'			=> __( 'Simple Table', 'reverie' ),
		);

		foreach ($block_types as $name => $label) { ?>
			<?php
			$checked = '';
			if( in_array( $name, $saved_block_types ) ) {
				$checked = 'checked="checked"';
			}?>
			<input type="checkbox" name="block_types[]" value="<?php echo $name ?>" id="<?php echo 'block_' . $name; ?>" <?php echo $checked; ?>>
			<label for="<?php echo 'block_' . $name; ?>"><?php echo $label; ?></label>
			<br/>
		<?php } ?>
	</div>
    <?php submit_button( __( 'Save settings', 'reverie' ), 'primary', 'submit' ) ?>  
</form>  

