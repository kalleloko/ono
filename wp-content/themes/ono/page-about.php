<?php get_header(); ?>

<!-- Row for main content area -->
	<div class="" role="main">
		<?php
		$main_about_page = get_page_by_path('about-ono');
		?>
		<article class="row">
			
			<section class="columns small-12 large-8 main-about">
				<header>
					<h1 class="entry-title"><?php _e( 'About', 'reverie' ) ?></h1>
				</header>
				<?php while(has_sub_field("blocks_" . LANG, $main_about_page->ID)): ?>
					<?php
					//continue if the block type is not active
					$row_layout = get_row_layout();

					//print the content
					echo apply_filters( 'the_content', get_sub_field( 'text_' . LANG, $main_about_page->ID ) ); ?>
				<?php endwhile; ?>
			</section>

			<section class="columns small-12 large-4 city-contacts">
				<header>
					<h2 class="entry-title"><?php _e( 'Contact ONO', 'reverie' ) ?></h1>
				</header>
				<?php while(has_sub_field("city_contacts", $main_about_page->ID ) ): ?>
					<div class="city-contact">
						<h3><?php the_sub_field( 'heading', $main_about_page->ID ); ?></h3>
						<?php echo apply_filters( 'the_content', get_sub_field( 'contact_info', $main_about_page->ID ) ); ?>
					</div>
				<?php endwhile; ?>
			</section>
		</article>

		<article class="row ono-family">
			<header class="columns small-12">
				<h2 class="entry-title"><?php _e( 'The ONO Family', 'reverie' ) ?></h1>
			</header>
			<div class="ono-persons">
			<?php while(has_sub_field("person", $main_about_page->ID ) ): ?>
				<section class="columns ono-person">
					<?php
					//continue if the block type is not active
					$img_id = get_sub_field( 'image', $main_about_page->ID );
					echo wp_get_attachment_image( $img_id, 'fourcol-square' );
					?>
					<h3 class="ono-name"><?php echo get_sub_field( 'name', $main_about_page->ID ); ?></h3>
					
					<p class="ono-link">
						<?php
						$link = get_sub_field( 'website', $main_about_page->ID );
						$link = format_link( $link );
						?>
						<a href="<?php echo $link['http']; ?>">
							<?php echo $link['www']; ?>
						</a>
					</p>
					<div class="ono-person-descr">
						<?php echo apply_filters( 'the_content', get_sub_field( 'text', $main_about_page->ID ) ); ?>
					</div>
				</section>
			<?php endwhile; ?>
			</div>
		</article>
	</div>
		
<?php get_footer(); ?>