<?php get_header(); ?>

<!-- Row for main content area -->
	<div class="application row" role="main">
	
	<?php /* Start loop */ ?>
		
		<?php while (have_posts()) : the_post(); ?>
			<header class="columns">
				<?php $cities = json_decode(CITY_MENU_ORDER); ?>
				<h4 class="entry-title"><?php _e( 'Application One Night Only', 'reverie' ) ?> <?php echo $cities->{CURRENT_CITY_SLUG}; ?></h4>
			</header>

			<div class="columns large-4 show-for-small">
				<?php while(has_sub_field("blocks_" . LANG)): ?>
					<?php
					//print the content
					echo apply_filters( 'the_content', get_sub_field( 'text_' . LANG ) ); ?>
				<?php endwhile; ?>
			</div>

			<section class="columns large-8">

				<?php
					if( is_localhost() ) {
						$form_id = 1;
					} else {
						switch(CURRENT_CITY_SLUG) {
							case 'oslo':
								$form_id = 4;
								break;

							case 'vilnius':
								$form_id = 6;
								break;

							case 'zurich':
								$form_id = '';
								break;
						}
					}
					if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( $form_id ); }
				?>

			</section>

			<div class="columns large-4 hide-for-small">
				<?php while(has_sub_field("blocks_" . LANG)): ?>
					<?php
					//print the content
					echo apply_filters( 'the_content', get_sub_field( 'text_' . LANG ) ); ?>
				<?php endwhile; ?>
			</div>
		<?php endwhile; // End the loop ?>
	</div>
		
<?php get_footer(); ?>