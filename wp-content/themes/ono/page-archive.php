<?php get_header(); ?>

<!-- Row for main content area -->
	<div class="" role="main">
	
	<?php /* Start page loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		
		<?php
		$sort_by = isset($_GET['sort-by'])? $_GET['sort-by'] : 'date';
		$show = isset($_GET['show'])? $_GET['show'] : 'all';
		$city = isset($_GET['city'])? $_GET['city'] : CURRENT_CITY_SLUG;

		$result = ono_get_archive_results( $sort_by, $show, $city);
		?>

		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>
			<div class="entry-content row">
				<div class="archive-navi columns large-4">
					<ul class="archive-navi-main">

						<?php
						$class = '';
						if( $sort_by == 'date' ) {
							$class = ' open';
						}
						?>
						<li class="sorting-li<?php echo $class; ?>">
							<a class="main-sort-link" href="<?php echo add_query_arg(array('sort-by' => 'date', 'show' => 'all')); ?>"><?php _e( 'Sort by Date', 'reverie' ) ?></a>
							<span class="icon arrow-down"></span>
							<ul class="sub-sort">
								<?php
								$class = '';
								if( $sort_by == 'date' && $show == 'all' ) {
									$class = ' active';
								}
								?>
								<li class="show-all-li<?php echo $class; ?>">
									<a href="<?php echo add_query_arg(array('sort-by' => 'date', 'show' => 'all')); ?>"><?php _e( 'Show all', 'reverie' ) ?></a>
								</li>
								<?php
								
								echo $result['date_nav'];
								?>
							</ul>
						</li>

						<?php
						$class = '';
						if( $sort_by == 'artist' ) {
							$class = ' open';
						}
						?>
						<li class="sorting-li<?php echo $class; ?>">
							<a class="main-sort-link" href="<?php echo add_query_arg(array('sort-by' => 'artist', 'show' => 'all')); ?>"><?php _e( 'Sort by Artist', 'reverie' ) ?></a>
							<span class="icon arrow-down"></span>
							
							<ul class="sub-sort letter">
								<?php
								$class = '';
								if( $sort_by == 'artist' && $show == 'all' ) {
									$class = ' active';
								}
								?>
								<li class="show-all-li<?php echo $class; ?>">
									<a href="<?php echo add_query_arg(array('sort-by' => 'artist', 'show' => 'all')); ?>"><?php _e( 'Show all', 'reverie' ) ?></a>
								</li>
								<?php
								echo $result['artist_nav'];
								?>
							</ul>
						</li>

						<?php
						$class = '';
						if( $sort_by == 'exhibition' ) {
							$class = ' open';
						}
						?>
						<li class="sorting-li<?php echo $class; ?>">
							<a class="main-sort-link" href="<?php echo add_query_arg(array('sort-by' => 'exhibition', 'show' => 'all')); ?>"><?php _e( 'Sort by Exhibition', 'reverie' ) ?></a>
							<span class="icon arrow-down"></span>
							<ul class="sub-sort letter">
								<?php
								$class = '';
								if( $sort_by == 'exhibition' && $show == 'all' ) {
									$class = ' active';
								}
								?>
								<li class="show-all-li<?php echo $class; ?>">
									<a href="<?php echo add_query_arg(array('sort-by' => 'exhibition', 'show' => 'all')); ?>"><?php _e( 'Show all', 'reverie' ) ?></a>
								</li>
								<?php
								echo $result['exhibition_nav'];
								?>
							</ul>
						</li>
					</ul>
					
					<ul class="city-filter sub-sort">
						<li><p><?php _e( 'Include results from', 'reverie' ) ?></p></li>
						<?php
						$cur = json_decode(CITY_MENU_ORDER);
			    		$class = array(
			    			'this_city'		=> $city == 'all' ? '' : 'class="active"',
			    			'all_cities'	=> $city == 'all' ? 'class="active"' : '',
			    		);
			    		$city_menu_order = json_decode(CITY_MENU_ORDER);
			    		?>
			    		<li <?php echo $class['this_city']; ?>>
			    			<a href="<?php echo add_query_arg(array('city' => CURRENT_CITY_SLUG)) ?> " data-filter="<?php echo CURRENT_CITY_SLUG ?>"><?php echo $city_menu_order->{CURRENT_CITY_SLUG}; ?></a>
			    		</li>
			    		<li <?php echo $class['all_cities']; ?>>
			    			<a href="<?php echo add_query_arg(array('city' => 'all')) ?> " data-filter="all"><?php _e( 'All cities', 'reverie' ) ?></a>
			    		</li>
					</ul>

				</div>


				<div class="columns large-8 artists-column">
					<ul class="archive-results">

						<?php
						echo $result['result'];
						?>
					
					</ul>
				</div>
			</div>
		</article>
	<?php endwhile; // End the page loop ?>

	</div>
		
<?php get_footer(); ?>