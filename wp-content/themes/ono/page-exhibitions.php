<?php get_header(); ?>
<!-- Row for main content area -->
	<div class="" role="main">
	
	<?php /* Start page loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="entry-content">
				<?php
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				if( $paged == 1 ) {
					$args = query_exhibitions('featured');
						
					$exhs = new WP_Query( $args );

					if( $exhs->have_posts() ): ?>
						<ul data-orbit data-options="pause_on_hover: false; slide_number: false;">
							<?php while ( $exhs->have_posts()) : $exhs->the_post(); ?>
								<?php get_template_part('content', 'exhibition-feat'); ?>
							<?php endwhile; ?>
						</ul>
					<?php 
					endif;
					wp_reset_query();
				} ?>

				<?php
				$args = query_exhibitions();
					
				$exhs = new WP_Query( $args );
				
				if( $exhs->have_posts() ): ?>
					<h4 class="past exh-type-header"><?php _e( 'Past Exhibitions', 'reverie' ); ?></h4>
					<div class="row past-exhibitions">
						<?php while ( $exhs->have_posts()) : $exhs->the_post(); ?>
							<?php get_template_part('content', 'exhibition'); ?>
						<?php endwhile; ?>
					</div>
				<?php
				endif;
				wp_reset_query();
				?>
				
					<?php
					// $exh_date = get_field('date');
					// $today = date('Ymd');
					// $one_month_ago = date('Ymd', strtotime('-1 month') );

					?>
				</div>
			</div>
			<footer>
				<?php /* Display navigation to next/previous pages when applicable */ ?>
				<?php if ( function_exists('reverie_pagination') ) { reverie_pagination( $exhs ); } else if ( is_paged() ) { ?>
					<nav id="post-nav">
						<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'reverie' ) ); ?></div>
						<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'reverie' ) ); ?></div>
					</nav>
				<?php } 
				wp_reset_query();
				?>
			</footer>
		</article>
	<?php endwhile; // End the page loop ?>

	</div>
		
<?php get_footer(); ?>