<?php
/*
Template Name: First Home
*/
get_header(); ?>

<!-- Row for main content area -->
	<div class="" role="main">
		<h1><img src="<?php echo site_url('/wp-content/themes/ono/img/One-Night-Only-logo-vit.png') ?>"/></h1>
		<div id="center-ul-wrap">
			<ul>
				<?php
				$city_menu_order = json_decode(CITY_MENU_ORDER);
		        $cities = get_terms('city');
		    	foreach ($city_menu_order as $slug => $city) {
		    		echo '<li>';
		    		echo '<a href="' . site_url( $slug . '/exhibitions' ) . '">' . $city . '</a>';
		    		echo '</li>';
		    	}
		    	?>
		    </ul>
		</div>
	    <?php while(has_sub_field("blocks_" . LANG)): ?>
			<?php
			//continue if the block type is not active
			$row_layout = get_row_layout();

			//print the content
			get_template_part( 'sections/section', $row_layout ); ?>
		<?php endwhile; ?>
	</div>
	<footer></footer>
		
<?php get_footer(); ?>