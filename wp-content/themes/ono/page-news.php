<?php get_header(); ?>

<!-- Row for main content area -->
	<div class="" role="main">
	
	<?php /* Start page loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<article <?php post_class('row') ?> id="post-<?php the_ID(); ?>">
			<div class="">
				<div class="entry-content large-8 columns">
					<header>
						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header>
					<?php
					$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
					/**
					 * The WordPress Query class.
					 * @link http://codex.wordpress.org/Function_Reference/WP_Query
					 *
					 */
					$args = array(
						//Choose ^ 'any' or from below, since 'any' cannot be in an array
						'post_type' => array(
							'post',
						),
						
						//Order & Orderby Parameters
						
						//Pagination Parameters
						'posts_per_page'         => 10,
						'paged' => $paged,
						
						//Taxonomy Parameters
						
						//Permission Parameters -
						'perm' => 'readable',

						
						//Parameters relating to caching
						'no_found_rows'          => false,
						'cache_results'          => true,
						'update_post_term_cache' => true,
						'update_post_meta_cache' => true,
						
					);
					
					$news = new WP_Query( $args );
					
					while ( $news->have_posts()) : $news->the_post(); ?>
						<?php get_template_part('content', 'news'); ?>
					<?php endwhile; // End the news loop ?>


					<footer>
						<?php /* Display navigation to next/previous pages when applicable */ ?>
						<?php if ( function_exists('reverie_pagination') ) { reverie_pagination( $news ); } else if ( is_paged() ) { ?>
							<nav id="post-nav">
								<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'reverie' ) ); ?></div>
								<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'reverie' ) ); ?></div>
							</nav>
						<?php } ?>
					</footer>
				</div>


				<aside class="columns large-4 latest-press">
					<?php
					$latest_press_page = get_page_by_path('latest-press');
					?>
					<header>
						<h4 class="entry-title"><?php _e( 'Featured elsewhere', 'reverie' ) ?></h4>
					</header>
					<section>
						<?php while(has_sub_field("blocks_" . LANG, $latest_press_page->ID)): ?>
							<?php
							//print the content
							echo apply_filters( 'the_content', get_sub_field( 'text_' . LANG, $latest_press_page->ID ) ); ?>
						<?php endwhile; ?>
					</section>
				</aside>
			</div>
		</article>
	<?php endwhile; // End the page loop ?>

	</div>
		
<?php get_footer(); ?>