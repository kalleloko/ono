<section class="image-section columns small-<?php the_sub_field( 'width_small' ); ?> large-<?php the_sub_field( 'width_large' ); ?>">
	<?php while ( has_sub_field( 'images_' . LANG ) ): ?>
		<?php
			$img_id = get_sub_field( 'image_' . LANG );
			$img_size = get_sub_field( 'image_size_' . LANG );
			echo wp_get_attachment_image( $img_id, $img_size );
		?>
	<?php endwhile; ?>
</section>