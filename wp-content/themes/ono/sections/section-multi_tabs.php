<section class="tabs-section columns small-<?php the_sub_field( 'width_small' ); ?> large-<?php the_sub_field( 'width_large' ); ?>">
	<?php
	$type = get_sub_field( 'type_' . LANG );
	switch ( $type ) {
		case 'accordion':
			$data_section = '="accordion"';
			break;

		case 'tabs':
			$data_section = '="tabs"';
			break;

		case 'vertical-tabs':
			$data_section = '="vertical-tabs"';
			break;

		default:
			$data_section = '';
			break;
	}
	?>
	<div class="section-container <?php echo $type; ?>" data-section<?php echo $data_section; ?>>
		<?php while ( has_sub_field( 'tabs_' . LANG ) ): ?>
			<section>
				<p class="title" data-section-title>
					<a href="#"><?php the_sub_field( 'title_' . LANG ) ?></a>
				</p>
				<div class="content" data-section-content>
					<?php echo apply_filters( 'the_content', get_sub_field( 'content_' . LANG ) ); ?>
				</div>
			</section>
			
		<?php endwhile; ?>
	</div>
</section>