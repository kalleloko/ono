<section class="orbit-section columns small-<?php the_sub_field( 'width_small' ); ?> large-<?php the_sub_field( 'width_large' ); ?>">
	<ul data-orbit>
		<!-- <div class="preloader"></div> -->
		<?php while ( has_sub_field( 'images_' . LANG ) ): ?>
			<?php
			$link = '';
			$linkend;
			if( get_sub_field( 'link_' . LANG ) ) {
				$link = '<a href="' . get_sub_field( 'link_' . LANG ) . '">';
				$linkend = '</a>';
			} ?>
			<li>
				<?php
				$img_id = get_sub_field( 'image_' . LANG );
				$img_size = get_sub_field( 'image_size_' . LANG );
				echo $link;
				echo wp_get_attachment_image( $img_id, $img_size );
				echo $linkend;
				?>
				<div class="orbit-caption"><?php the_sub_field( 'title_' . LANG ); ?></div>
			</li>
		<?php endwhile; ?>
	</ul>
</section>