<section class="table-section columns small-<?php the_sub_field( 'width_small' ); ?> large-<?php the_sub_field( 'width_large' ); ?>">

	<?php
	$table_rows = get_sub_field( 'table_rows_' . LANG );
	$last_row = count( $table_rows );
	
	$table_settings = get_sub_field( 'tablesettings_' . LANG );
	if( empty($table_settings) ) $table_settings = array();


	$celltype = 'td';
	$sectiontype = 'tbody';
	if( in_array('head', $table_settings) ) {
		$celltype = 'th';
		$sectiontype = 'thead';
	}
	?>

	<table>
		<?php
		$count = 1;
		echo "<$sectiontype>";
		while ( has_sub_field( 'table_rows_' . LANG ) ):

			if( in_array('foot', $table_settings) ) {
				if( $count == $last_row ) {
					echo "</$sectiontype>";
					$sectiontype = 'tfoot';
					echo "<$sectiontype>";
				}
			} ?>

			<tr>
				<?php
				
				echo "<$celltype>";
				echo str_replace('|', "</$celltype><$celltype>", get_sub_field( 'row_' . LANG ));
				echo "</$celltype>";

				?>
			</tr>
			<?php 
			if( in_array('head', $table_settings) ) {
				if( $count == 1 ) {
					$celltype = 'td';
					echo "</$sectiontype>";
					$sectiontype = 'tbody';
					echo "<$sectiontype>";
				}
			}
			
			?>
			<?php $count++ ?>
		<?php
		endwhile;
		echo "</$sectiontype>";
		?>
	</table>
</section>