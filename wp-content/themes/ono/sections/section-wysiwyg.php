<section class="text-section columns small-<?php the_sub_field( 'width_small' ); ?> large-<?php the_sub_field( 'width_large' ); ?>">
	<?php echo apply_filters( 'the_content', get_sub_field( 'text_' . LANG ) ); ?>
</section>