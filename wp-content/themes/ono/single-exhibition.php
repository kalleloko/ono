<?php get_header(); ?>

<!-- Row for main content area -->
	
	<div class="" role="main">
		<?php /* Start loop */ ?>
		<?php while (have_posts()) : the_post(); ?>

			<?php
			$artists = get_the_terms( get_the_id(), 'artist' );
			$aside = '<div>';
			$aside .= '<h4 class="more-from-artist">' . __( 'Share exhibition', 'reverie' ) . '</h4>';
			$aside .= ono_shareLinks( get_permalink() . __(' at ONO Gallery', 'reverie') );

			$related_exh = '';
			if( empty($artists) ) $artists = array();
			foreach ($artists as $artist) {

				$args = array(
					//Type & Status Parameters
					'post_type'   => 'exhibition',
					'posts_per_page' => -1,
					'tax_query' => array(
						array(
							'taxonomy' => 'artist',
							'field' => 'slug',
							'terms' => $artist->slug,
						)
					),

					'post__not_in' => array(
						get_the_ID(),
					),
					
					//Parameters relating to caching
					'no_found_rows'          => false,
					'cache_results'          => false,
					'update_post_term_cache' => false,
					'update_post_meta_cache' => false,
					
				);
				
				$artists_exh = new WP_Query( $args );
				if( $artists_exh->have_posts() ) {
					$related_exh .= '<p class="related-artists"><span class="related-artist">' . ono_name( $artist->name ) . '</span> ';
					$count = 0;
					while ( $artists_exh->have_posts()) : $artists_exh->the_post();
						$related_exh .= '<a href="' . get_permalink() . '">' . wp_trim_words(get_the_title(), 6) . '</a>';
						$count++;
					endwhile;
					$related_exh .= '</p>';
				}
				wp_reset_query();
			}

			if( !empty($related_exh) ) {
				$aside .= '<h4 class="more-from-artist">' . __( 'Related exhibitions', 'reverie' ) . '</h4>';
				$aside .= $related_exh;
			}
			$aside .= '</div>';
			
			?>

			<article <?php post_class('row') ?> id="post-<?php the_ID(); ?>">
				<div class="columns small-12 large-4">
					<header>
						<h1 class="entry-title"><?php _e( 'Exhibition', 'reverie' ) ?></h1>
					</header>
					<div class="entry-content">
						<?php
						$the_artists = artist_list($artists, '', 100);
						?>
						<h4 class="single-artists"><?php echo $the_artists; ?></h4>
						<h4 class="exh-title"><?php the_title(); ?></h4>
						<div class="exh-meta">
							<p>
								<?php
								echo_date( get_field('date') );
								echo ' ' . get_field('time') .br();
								echo get_field( 'address' ) . br();
								?>
							</p>
						</div>
						<div class="intro-text"><?php echo apply_filters( 'the_content', get_field( 'intro-text' ) ); ?></div>
						<div class="description"><?php echo apply_filters( 'the_content', get_field( 'description' ) ); ?></div>
					</div>
					<aside class="hide-for-small">
						<?php echo $aside; ?>
					</aside>
				</div>
				<div class="columns small-12 large-8 images">
					<?php
					if( !has_sub_field('extra_images') && !has_sub_field('videos') ) {
						echo wp_get_attachment_image( get_field('featured_image'), 'eightcol' );
					}
					?>
					<?php 
					while ( has_sub_field('videos') ) {
						echo get_sub_field('video');
					}
					while ( has_sub_field('extra_images') ) {
						echo wp_get_attachment_image( get_sub_field('image'), 'eightcol' );
					} ?>
				</div>
				
				<aside class="columns small-12 show-for-small">
					<?php echo $aside; ?>
				</aside>

			</article>
		<?php endwhile; // End the loop ?>
	</div>

<?php get_footer(); ?>